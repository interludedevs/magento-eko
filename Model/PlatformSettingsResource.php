<?php

declare(strict_types=1);

namespace EkoLabs\Eko\Model;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class PlatformSettingsResource extends AbstractDb
{
    /**
     * ProductAttributesResource Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('eko_platform_settings', 'id');
    }
}
