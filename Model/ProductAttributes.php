<?php

declare(strict_types=1);

namespace EkoLabs\Eko\Model;

use Magento\Framework\Model\AbstractModel;
use EkoLabs\Eko\Api\Data\ProductAttributesInterface;

class ProductAttributes extends AbstractModel implements ProductAttributesInterface
{
    public const EKO_GALLERY_OBJ = 'eko_gallery_obj';
    public const PRODUCT_ID = 'product_id';
    public const ID = 'id';

    /**
     * ProductAttributes constructor
     */
    protected function _construct()
    {
        $this->_init(ProductAttributesResource::class);
    }

    /**
     * Get the value of the id column
     *
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->getData(self::ID);
    }

    /**
     * Get the value of the product_id column
     *
     * @return int
     */
    public function getProductId(): int
    {
        return (int) $this->getData(self::PRODUCT_ID);
    }

    /**
     * Get the value of the eko_gallery_obj column
     *
     * @return string
     */
    public function getEkoGalleryObject(): string
    {
        return (string) $this->getData(self::EKO_GALLERY_OBJ);
    }

    /**
     * Set the value of the product_id column
     *
     * @param int $value
     * @return ProductAttributesInterface
     */
    public function setProductId(int $value): ProductAttributesInterface
    {
        return $this->setData(self::PRODUCT_ID, $value);
    }

    /**
     * Set the value of the eko_gallery_obj column
     *
     * @param string $value
     * @return ProductAttributesInterface
     */
    public function setEkoGalleryObject(string $value): ProductAttributesInterface
    {
        return $this->setData(self::EKO_GALLERY_OBJ, $value);
    }
}
