<?php
declare(strict_types=1);

namespace EkoLabs\Eko\Model;

use Magento\Framework\Model\AbstractModel;
use EkoLabs\Eko\Api\Data\PlatformSettingsInterface;

class PlatformSettings extends AbstractModel implements PlatformSettingsInterface
{
    public const STORE_ID = 'store_id';
    public const SETTING_VALUE = 'setting_value';
    public const ID = 'id';

    /**
     * PlatformSettings constructor
     */
    protected function _construct()
    {
        $this->_init(PlatformSettingsResource::class);
    }

    /**
     * Get the value of the id column
     *
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->getData(self::ID);
    }

    /**
     * Get the value of the store_id column
     *
     * @return int
     */
    public function getStoreId(): string
    {
        return (string) $this->getData(self::STORE_ID);
    }

    /**
     * Get the value of the value column
     *
     * @return string
     */
    public function getValue(): string
    {
        return (string) $this->getData(self::SETTING_VALUE);
    }

    /**
     * Set the value of the store_id column
     *
     * @param int $value
     * @return PlatformSettingsInterface
     */
    public function setStoreId(string $value): PlatformSettingsInterface
    {
        return $this->setData(self::STORE_ID, $value);
    }

    /**
     * Set the value of the eko_gallery_obj column
     *
     * @param string $value
     * @return PlatformSettingsInterface
     */
    public function setValue(string $value): PlatformSettingsInterface
    {
        return $this->setData(self::SETTING_VALUE, $value);
    }
}
