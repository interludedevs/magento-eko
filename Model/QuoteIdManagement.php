<?php
namespace EkoLabs\Eko\Model;

use EkoLabs\Eko\Api\QuoteIdManagementInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

class QuoteIdManagement implements QuoteIdManagementInterface
{
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * QuoteIdManagement constructor
     *
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(CheckoutSession $checkoutSession)
    {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Get current quote ID
     *
     * @return string|null
     */
    public function getQuoteId()
    {
        $quote = $this->checkoutSession->getQuote();
        return $quote ? $quote->getId() : null;
    }
}
