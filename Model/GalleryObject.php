<?php
namespace EkoLabs\Eko\Model;

use EkoLabs\Eko\Api\Data\GalleryObjectInterface;
use Magento\Framework\View\Asset\Repository as AssetRepository;

class GalleryObject implements GalleryObjectInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $obj;

    /**
     * @var string
     */
    private $galleryVersion;

    /**
     * @var AssetRepository
     */
    private AssetRepository $assetRepository;

    /**
     * GalleryObject constructor
     *
     * @param array $data
     * @param AssetRepository $assetRepository
     * @param string|null $galleryVersion
     */
    public function __construct(
        $data,
        AssetRepository $assetRepository,
        $galleryVersion = null
    ) {
        $this->assetRepository = $assetRepository;
        $this->obj = null;
        if ($data) {
            $this->obj = json_decode($data, true);
            $this->conventPathObjectToArray('style');
            $this->conventPathObjectToArray('layout.desktop.config');
            $this->conventPathObjectToArray('layout.mobile.config');
        }

        $this->galleryVersion = $galleryVersion;
    }

    /**
     * Get the gallery object
     *
     * @return array|null
     */
    public function toJson() : ?array
    {
        return $this->obj;
    }

    /**
     * Check if the gallery has an experiment configuration
     *
     * @param string|null $type
     * @return boolean
     */
    public function hasActiveExperiment($type = null) : bool
    {
        $experiment = $this->getActiveExperiment($type);
        return ($experiment != null);
    }

    /**
     * Get the experiment configuration if applicable
     *
     * @param string|null $type
     * @return array|null
     */
    public function getActiveExperiment($type = null) : ?array
    {
        $result = null;
        $currentTimestamp = time();

        if (is_array($this?->obj['experiments'] ?? null)) {
            foreach ($this->obj['experiments'] as $experiment) {
                $startTimestamp = strtotime($experiment['startDate']);
                $endTimestamp = strtotime($experiment['endDate']);

                if ($experiment['active'] &&
                    $currentTimestamp >= $startTimestamp &&
                    $currentTimestamp < $endTimestamp &&
                    ($type ? $experiment['type'] === $type : true)
                ) {
                    $result = $experiment;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * Get the gallery CSS URL
     *
     * @return string
     */
    public function getCssUrl() : string
    {
        return $this->getGalleryUrl('css');
    }

    /**
     * Get the gallery JS URL
     *
     * @return string
     */
    public function getJsUrl() : string
    {
        return $this->getGalleryUrl('js');
    }

    /**
     * Get the URL of a gallery asset
     *
     * @param string $ext
     * @return string
     */
    private function getGalleryUrl($ext)
    {
        $useThemeAssets = $this->obj['options']['useThemeAssets'] ?? true;
        $result = $useThemeAssets ?
            $this->getGalleryUrlFromMagento($ext) :
            $this->getGalleryUrlFromEko($ext);
        return $result;
    }

    /**
     * Get the URL of a gallery asset from Eko (js or css)
     *
     * @param string $ext
     * @return string
     */
    private function getGalleryUrlFromEko($ext)
    {
        return !$this->obj ? '' : sprintf(
            'https://%splay.eko.com/components/eko-gallery/%seko-%s.%s',
            $this->obj['options']['galleryEnv'] ?? '',
            $this->galleryVersion ? ($this->galleryVersion . '/') : '',
            $this->obj['options']['galleryName'] ?? 'gallery',
            $ext
        );
    }

    /**
     * Get the URL of a local gallery asset (js or css)
     *
     * @param string $ext
     * @return string
     */
    private function getGalleryUrlFromMagento($ext)
    {
        $result = null;
        try {
            $galleryName = $this->obj['options']['galleryName'] ?? 'gallery';
            $path = 'EkoLabs_Eko/static/' . rawurlencode('eko-' . $galleryName . '.' . $ext);
            $result = $this->assetRepository->getUrlWithParams($path, []);
        } catch (Exception $e) {
            $result = '';
        }
        return $result;
    }

    /**
     * Convert an inner object at $path to an array of objects
     *
     * @param string $path
     * @return void
     */
    private function conventPathObjectToArray($path)
    {
        $value = $this->getValue($path, null);
        if ($value) {
            $value = $this->convertObjectToArray($value);
            $this->setValue($path, $value);
        }
    }

    /**
     * Convert an object to an array of objects with 'name' and 'value' attributes
     *
     * @param array $obj
     * @return array
     */
    private function convertObjectToArray($obj)
    {
        $result = [];
        foreach ($obj as $name => $value) {
            if (is_object($value) || is_array($value)) {
                $result[$name] = $value;
            } else {
                $result[]= [ 'name' => $name, 'value' => $value ];
            }
        }
        return $result;
    }

    /**
     * Get value of inner object using a dot convension
     *
     * @param string $path
     * @param string|integer|boolean|array $fallback
     * @return string|integer|boolean|array
     */
    private function getValue($path, $fallback)
    {
        $keys = explode('.', $path);
        $value = $this->obj;
    
        foreach ($keys as $key) {
            if (isset($value[$key])) {
                $value = $value[$key];
            } else {
                return $fallback;
            }
        }
        return $value;
    }

    /**
     * Set value of inner object using a dot convension
     *
     * @param string $path
     * @param string|integer|boolean|array $value
     * @return void
     */
    private function setValue($path, $value)
    {
        $keys = explode('.', $path);
        $current = &$this->obj;
        
        foreach ($keys as $key) {
            if (!isset($current[$key]) || !is_array($current[$key])) {
                $current[$key] = [];
            }
            $current = &$current[$key];
        }
        $current = $value;
    }
}
