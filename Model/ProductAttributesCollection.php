<?php

declare(strict_types=1);

namespace EkoLabs\Eko\Model;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class ProductAttributesCollection extends AbstractCollection
{
    /**
     * ProductAttributesCollection Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ProductAttributes::class, ProductAttributesResource::class);
    }
}
