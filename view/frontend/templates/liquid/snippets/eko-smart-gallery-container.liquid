{% comment %}
    Props:
        name
        deliverableId
        queryparams
        globalQueryparams
        variantId
        env
        ekoLoading
        carouselOptions
        activeExperiment
{% endcomment %}

{% comment %} Assign Default Values {% endcomment %}
{% capture deliveryPrefix %}https://{{ env | default: '' }}play.eko.com/{% endcapture %}

{% comment %}
    TODO: remove this and use layout variables in css.
{% endcomment %}
{% assign width = carouselOptions.item.width | default: 600 %}
{% assign height = carouselOptions.item.height | default: 600 %}

{% assign ekoLoadingQP = '' %}
{% if ekoLoading and ekoLoading != '' %}
    {% capture ekoLoadingQP %}&eko_loading={{ ekoLoading }}{% endcapture %}
{% endif %}

{% assign ekoFallbackQP = '' %}
{% if ekoFallback %}
    {% capture ekoFallbackQP %}&_eko_fallback={{ ekoFallback }}{% endcapture %}
{% endif %}

{% assign additionalQueryParams = '' %}
{% for param in queryparams %}
    {% capture additionalQueryParams %}{{ additionalQueryParams }}&{{ param[0] }}={{ param[1] }}{% endcapture %}
{% endfor %}
{% assign additionalQueryParams = additionalQueryParams | append: globalQueryparams %}

<div class="eko-smart-gallery-container eko-project-not-ready"
     data-type="smart-gallery"
     data-qp="{{ additionalQueryParams | url_escape }}"
     aria-label="smart gallery"
        {% if name %}
            data-name="{{ name }}"
        {% endif %}
        {% assign additionalHashParams = '' %}
{% if variantId != false %}
    data-variant="{{ variantId }}"
     {% capture additionalHashParams %}{{ additionalHashParams }}#variantId={{ variantId }}{% endcapture %}
     {% endif %}
     {% if deliverableId %}
     data-deliverableid="{{ deliverableId }}"
        {% endif %}
        {% if device %}
            data-device="{{ device }}"
        {% endif %}
>
    {% capture interactiveUrl %}{{ deliveryPrefix }}embed?did={{ deliverableId }}&nocover=true&galleryBlockMode=gallery&eko_pixel=true{{ ekoFallbackQP }}{{ additionalQueryParams }}{% endcapture %}
    <iframe allow="autoplay; fullscreen"
            {% if activeExperiment.type == "deliverable" %}
                class="eko-hidden"
            {% endif %}
            data-variant="CONTROL"
            src="{{ interactiveUrl | url_escape }}{{ ekoLoadingQP }}{{ additionalHashParams }}"
            style="border: 0;"></iframe>
    {% if activeExperiment.type == "deliverable" %}
        {% for variant in activeExperiment.variants %}
            {% capture interactiveVariantUrl %}{{ deliveryPrefix }}embed?did={{ variant.params.deliverableId }}&nocover=true&galleryBlockMode=gallery&eko_pixel=true{{ ekoFallbackQP }}{{ additionalQueryParams }}{% endcapture %}
            <iframe allow="autoplay; fullscreen"
                    class="eko-hidden"
                    data-variant="{{ variant.name }}"
                    src="{{ interactiveVariantUrl | url_escape }}{{ ekoLoadingQP }}{{ additionalHashParams }}"
                    style="border: 0;"></iframe>
        {% endfor %}
    {% endif %}
</div>
