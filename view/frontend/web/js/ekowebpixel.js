'use strict';

// eslint-disable-next-line no-undef
define(['Magento_Customer/js/customer-data', 'mage/url'], function(customerData, urlBuilder) {
    const experimentId = 'eko.experiment';
    const metadataId = 'eko.metadata';
    const taId =  'eko.ta';

    let cart = {};
    let cartId = null;
    let cartItems = {};
    let currency = {};
    let ekoData = {};
    let debugLog = false;

    function log(severity, message, obj) {
        if (!debugLog) {
            return;
        }
        // eslint-disable-next-line no-console
        console[severity](message, obj);
    }

    async function fetchCartIdFromApi() {
        let result = null;
        const serviceUrl = urlBuilder.build('rest/V1/eko/quote-id?ck=' + Date.now());
        try {
            const response = await fetch(serviceUrl, {
                method: 'GET',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            if (response.ok) {
                result = await response.json();
                if (Array.isArray(result)) {
                    result = null;
                }
            }
        } catch (error) {
            log('warn', '[ekowebpixel::fetchCartIdFromApi] failed fetching quote id', error);
        }
        return result;
    }

    function parseDataObject(dataObject) {
        if (!dataObject) {
            return {};
        }
        try {
            dataObject = JSON.parse(dataObject);
        } catch (error) {
            log('warn', '[ekowebpixel::parseDataObject] failed parsing data object', error);
            dataObject = {};
        }
        return dataObject;
    }

    function getCustomerMetadataFromItems(data, metadata) {
        if (!data?.items) {
            return;
        }

        const foundItemWithMetadata = data.items.find((item) => item.item_id &&
            metadata[item.item_id] &&
            metadata[item.item_id].customer);

        if (!foundItemWithMetadata) {
            return;
        }

        return metadata[foundItemWithMetadata.item_id].customer;
    }

    function getEkoData() {
        return {
            experiment: parseDataObject(localStorage.getItem(experimentId)),
            metadata: parseDataObject(localStorage.getItem(metadataId)),
            ta: parseDataObject(localStorage.getItem(taId))
        };
    }

    function setEkoData(key, dataObject) {
        if (!dataObject || !key) {
            return false;
        }
        try {
            dataObject = JSON.stringify(dataObject);
            localStorage.setItem(key, dataObject);
        } catch (error) {
            log('warn', '[ekowebpixel::setEkoData] failed saving data object', error);
            return false;
        }
        return true;
    }

    function createEkoItem(magentoItem) {
        const ekoProduct = ekoData?.metadata[magentoItem?.product_id]?.product;

        /* eslint-disable camelcase */
        let item = {
            id: ekoProduct?.id,
            sku: ekoProduct?.sku,
            upc: ekoProduct?.upc,
            name: ekoProduct?.name,
            typeid: ekoProduct?.type,
            item_id: magentoItem?.product_id,
            item_name: magentoItem?.product_name,
            variant_id: magentoItem?.product_sku,
            quantity: parseInt(magentoItem?.qty || 0, 10),
            item_price: {
                amount: parseInt(magentoItem?.product_price_value || 0, 10),
                currency: currency?.code
            }
        };
        /* eslint-enable camelcase */

        return item;
    }

    function stripEkoItem(ekoItem) {
        const keys = ['id', 'sku', 'upc', 'name', 'typeid'];
        const result = structuredClone(ekoItem);
        keys.map((key) => {
            return delete result[key];
        });
        return result;
    }

    function stripVariantId(ekoItem) {
        const result = structuredClone(ekoItem);
        delete result.variant_id;
        return result;
    }

    function registerContextPackers() {
        let contextPackers = [
            {
                schema: 'iglu:com.helloeko/page/jsonschema/1-0-0',
                match: /^pixel\..*$/,
                getData: function() {
                    return {
                        url: window.location.href || '',
                        referrer: document.referrer || '',
                        title: document.title || ''
                    };
                }
            },
            {
                match: /^pixel.cart.\D+/,
                schema: 'iglu:com.helloeko/cart/jsonschema/1-0-1',
                getData(data) {
                    let result = structuredClone(data);
                    result.items = result.items.map(stripEkoItem);
                    result.items = result.items.map(stripVariantId);
                    return result;
                }
            },
            {
                match: /(\w*)pixel\.(checkout|order)/,
                schema: 'iglu:com.helloeko/checkout/jsonschema/1-0-3',
                getData(data) {
                    let result = structuredClone(data);
                    delete result.order_id;
                    result.items = result.items.map(stripEkoItem);
                    result.items = result.items.map(stripVariantId);
                    return result;
                }
            },
            {
                match: /(\w*)pixel.order/,
                schema: 'iglu:com.helloeko/order/jsonschema/1-0-1',
                getData(data) {
                    let result = structuredClone(data);
                    delete result.checkout_id;
                    result.items = result.items.map(stripEkoItem);
                    result.items = result.items.map(stripVariantId);
                    return result;
                }
            },
            {
                match: /(\w*)pixel.paymentinfo/,
                schema: 'iglu:com.helloeko/paymentinfo/jsonschema/1-0-0',
                getData(data) {
                    let result = structuredClone(data);
                    result.items = result.items.map(stripEkoItem);
                    result.items = result.items.map(stripVariantId);
                    return result;
                }
            },
            {
                match: 'trafficallocation.decision',
                schema: 'iglu:com.helloeko/traffic_allocation/jsonschema/1-0-0',
                getData(data) {
                    if (!data) {
                        return;
                    }
                    const packedData = {
                        decision: data.decision
                    };
                    if (data.config) {
                        packedData.config = data.config;
                    }
                    if (!isNaN(data.traffic_split)) {
                        // eslint-disable-next-line camelcase
                        packedData.traffic_split = data.traffic_split;
                    }
                    if (data.provider) {
                        packedData.provider = data.provider;
                    }
                    return packedData;
                }
            },
            {
                match: 'product.linkout',
                schema: 'iglu:com.helloeko/product_linkout/jsonschema/1-0-0',
                getData: function(data) {
                    if (!data) {
                        return;
                    }
                    const result = {
                        url: data.url || '',
                        itemid: data?.product?.item_id || '',
                        itemname: data?.product?.item_name || '',
                    };
                    return result;
                }
            },
        ];

        for (const contextPacker of contextPackers) {
            // eslint-disable-next-line new-cap
            window.EkoAnalytics('registerContextPacker', { ...contextPacker, tag: 'ekowebpixel' });
        }
    }

    function registerConditionalContextPackers(metadata, experiment) {
        let contextPackers = [
            {
                match: /^pixel\..*/,
                schema: 'iglu:com.helloeko/experiment/jsonschema/1-0-3',
                getData(data) {
                    if (!data?.items) {
                        return;
                    }

                    const foundItemWithExperiment = data.items.find((item) => item.item_id && experiment[item.item_id]);
                    if (!foundItemWithExperiment) {
                        return;
                    }

                    const currentProductExperiment = experiment[foundItemWithExperiment.item_id];

                    let experimentData = {
                        experimentid: String(currentProductExperiment.id || ''),
                        experimenttype: String(currentProductExperiment.type || ''),
                        experimentvariant: String(currentProductExperiment.variant || ''),
                    };

                    let targetDevice = currentProductExperiment.targetDevice?.toLowerCase();

                    if (targetDevice === 'mobile' || targetDevice === 'notmobile') {
                        experimentData.experimenttargeting = {
                            device: targetDevice
                        };
                    }

                    return experimentData;
                }
            },
            {
                match: /^pixel\..*/,
                schema: 'iglu:com.helloeko/asset/jsonschema/1-0-0',
                getData(data) {
                    if (!data?.items) {
                        return;
                    }

                    const foundItemWithMetadata = data.items.find((item) => item.item_id &&
                                                                            metadata[item.item_id] &&
                                                                            metadata[item.item_id].assets &&
                                                                            metadata[item.item_id].assets[0]);
                    if (!foundItemWithMetadata) {
                        return;
                    }

                    let asset = metadata[foundItemWithMetadata.item_id].assets[0];

                    return {
                        type: String(asset?.type || ''),
                        deliverableid: String(asset?.deliverableId || ''),
                        name: String(asset?.name || ''),
                        artifactid: '',
                    };
                }
            },
            {
                schema: 'iglu:com.helloeko/organization/jsonschema/1-0-0',
                match: /^pixel\..*/,
                getData: function(data) {
                    const customer = getCustomerMetadataFromItems(data, metadata);

                    if (!customer) {
                        return;
                    }

                    return { id: String(customer.organizationId || '') };
                }
            },
            {
                schema: 'iglu:com.helloeko/space/jsonschema/1-0-2',
                match: /^pixel\..*/,
                getData: function(data) {
                    const customer = getCustomerMetadataFromItems(data, metadata);

                    if (!customer) {
                        return;
                    }

                    return { id: String(customer.spaceId || '') };
                }
            },
            {
                schema: 'iglu:com.helloeko/variant/jsonschema/1-0-1',
                match: /^pixel\..*/,
                getData: function(data) {
                    if (!data?.items) {
                        return;
                    }

                    const foundItemWithMetadata = data.items.find((item) => item.item_id &&
                                                                            metadata[item.item_id]);
                    if (!foundItemWithMetadata) {
                        return;
                    }

                    return {
                        id: String(foundItemWithMetadata.variant_id || ''),
                    };
                }
            },
            {
                // eslint-disable-next-line max-len
                match: /^pixel\.(product(\..*)?|checkout(\..*)?|cart(\..*)?|order(\..*)?|waitlist(\..*)?|product_viewed(\..*)?)$/,
                schema: 'iglu:com.helloeko/products/jsonschema/1-0-1',
                getData(data) {
                    let result = structuredClone(data);
                    if (result.items) {
                        result.items = result.items.map(stripVariantId);
                    } else {
                        result.items = [];
                    }
                    return {
                        items: result.items
                    };
                }
            }
        ];

        for (const contextPacker of contextPackers) {
            // eslint-disable-next-line new-cap
            window.EkoAnalytics('registerContextPacker', { ...contextPacker, tag: 'ekowebpixel' });
        }
    }

    function trackCart(eventName, cartData) {
        /* eslint-disable camelcase */
        const eventData = {
            checkout_id: String(cartId || ''),
            order_id: String(cartData?.order_id || ''),
            total_price: {
                amount: parseInt(cartData?.subtotalAmount || 0, 10),
                currency: currency?.code
            },
            total_quantity: parseInt(cartData?.summary_count || 0, 10),
            items: cartData?.items?.map(createEkoItem) || []
        };
        /* eslint-enable camelcase */

        // eslint-disable-next-line new-cap
        window.EkoAnalytics('track', eventName, eventData);
        log('log', '[ekowebpixel::trackCart]' + eventName, eventData);
    }

    function trackProductView(eventName, product) {
        if (!product) {
            return;
        }
        /* eslint-disable camelcase */
        const eventData = {
            items: [createEkoItem(product)]
        };
        /* eslint-enable camelcase */

        // eslint-disable-next-line new-cap
        window.EkoAnalytics('track', eventName, eventData);
        log('log', '[ekowebpixel::trackProductView] ' + eventName, eventData);
    }

    function trackPageView(eventName) {
        const eventData = {};
        // eslint-disable-next-line new-cap
        window.EkoAnalytics('track', eventName, eventData);
        log('log', '[ekowebpixel::trackPageView] ' + eventName, eventData);
    }

    function trackProductLinkout(product, url) {
        if (!product) {
            return;
        }

        /* eslint-disable camelcase */
        const eventData = {
            url,
            product: createEkoItem(product),
        };
        /* eslint-enable camelcase */

        const eventName = 'product.linkout';

        // eslint-disable-next-line new-cap
        window.EkoAnalytics('track', eventName, eventData);
        log('log', '[ekowebpixel::trackProductLinkout] ' + eventName, eventData);
    }

    async function onCartUpdate(updatedCart) {
        let updatedCartItems = {};
        let eventName;
        let items = [];

        updatedCart?.items?.forEach(function(i) {
            updatedCartItems[i.product_sku] = i.qty;
        });

        Object.keys(updatedCartItems).forEach(function(productId) {
            if (!cartItems[productId] || updatedCartItems[productId] > cartItems[productId]) {
                eventName = 'pixel.cart.add';
                items.push(updatedCart?.items?.find((i) => i.product_sku === productId));
            }
        });
        Object.keys(cartItems).forEach(function(productId) {
            if (!updatedCartItems[productId] || updatedCartItems[productId] < cartItems[productId]) {
                eventName = 'pixel.cart.remove';
                items.push(cart?.items?.find((i) => i.product_sku === productId));
            }
        });

        /* eslint-disable camelcase */
        const eventData = {
            // first add to cart creates the cart so we need to fetch that info again from the server
            cart_id: String(cartId || await fetchCartIdFromApi() || ''),
            total_price: {
                amount: parseInt(updatedCart?.subtotalAmount || 0, 10),
                currency: currency?.code
            },
            total_quantity: parseInt(updatedCart?.summary_count || 0, 10),
            items: items.map(createEkoItem)
        };
        /* eslint-enable camelcase */

        if (eventName) {
            // eslint-disable-next-line new-cap
            window.EkoAnalytics('track', eventName, eventData);
            log('log', '[ekowebpixel::onCartUpdate]' + eventName, eventData);
        }

        cartItems = updatedCartItems;
        cart = updatedCart;
    }

    function trackTrafficAllocationDecision(trafficAllocation) {
        if (trafficAllocation && trafficAllocation.decision && !trafficAllocation.tracked) {
            // eslint-disable-next-line new-cap
            window.EkoAnalytics('track', 'trafficallocation.decision', trafficAllocation);
            trafficAllocation.tracked = true;
            setEkoData(taId, trafficAllocation);
        }
    }

    return async function(config) {
        if (!window.EkoAnalytics) {
            return;
        }

        debugLog = config.debugLog;
        ekoData = getEkoData();

        // register context packers
        registerContextPackers();
        if (config.loadEA) {
            // if the webpixel is responsible for loading EA it should register extra context packers
            registerConditionalContextPackers(ekoData.metadata, ekoData.experiment);
        }

        // track traffic allocation decision
        trackTrafficAllocationDecision(ekoData.ta);

        // store current settings
        currency = config.currency;

        // initialize the current cart and items objects
        let cartObserver = customerData.get('cart');
        cart = cartObserver();
        cart?.items?.forEach(function(item) {
            cartItems[item.product_sku] = item.qty;
        });
        // subscribe the changes in the cart
        cartObserver.subscribe(onCartUpdate, 'updateItem');

        // get the cart data
        let cartData = cart;
        if (config.event === 'pixel.order' && config.order) {
            // when order is complete we don't have an active cart anymore
            // in order to track what *was* included in the order. the cart info
            // related to the order is provided from the server side via the config
            cartData = config.order;
            cartId = config.order.checkout_id;
        } else {
            cartId = await fetchCartIdFromApi();
        }

        // track product linkout if needed
        if (config.productLinkoutSelector) {
            try {
                const els = document.querySelectorAll(config.productLinkoutSelector);
                els.forEach((el) => {
                    el.addEventListener('click', () => {
                        trackProductLinkout(config.product, el.getAttribute('href'));
                    });
                });
            } catch (e) {
                log('error', '[ekowebpixel::init] - selector is not valid');
            }
        }

        // track page view
        trackPageView('pixel.page_viewed');

        // additional tracking
        if (config.event === 'pixel.product_viewed') {
            trackProductView(config.event, config.product);
        } else {
            trackCart(config.event, cartData);
        }
    };
});
