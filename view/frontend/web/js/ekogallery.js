'use strict';

// eslint-disable-next-line no-undef
define([
    'jquery',
    'underscore',
    'uiClass'
], function($, _, Class) {
    return Class.extend({

        defaults: {
            settings: {},
            config: {},
            startConfig: {}
        },

        /**
         * Initializes gallery.
         * @param {Object} config - Gallery configuration.
         * @param {String} element - String selector of gallery DOM element.
         */
        initialize: function(config, element) {
            this._super();

            this.config = config;

            this.settings = {
                $element: $(element),
                $pageWrapper: $('body>.page-wrapper'),
                currentConfig: config,
                defaultConfig: _.clone(config),
                fullscreenConfig: _.clone(config.fullscreen),
                breakpoints: config.breakpoints,
                activeBreakpoint: {},
                fotoramaApi: null,
                isFullscreen: false,
                api: null,
                data: _.clone(config.data)
            };

            $.extend(true, this.startConfig, config);

            this.initApi();
        },

        /**
         * Creates gallery's API.
         */
        initApi: function() {
            let settings = this.settings;
            let api = {
                /**
                 * Contains fotorama's API methods.
                 */
                fotorama: settings.fotoramaApi,

                /**
                 * Displays the last image on preview.
                 */
                last: function() {},

                /**
                 * Displays the first image on preview.
                 */
                first: function() {},

                /**
                 * Displays previous element on preview.
                 */
                prev: function() {},

                /**
                 * Displays next element on preview.
                 */
                next: function() {},

                /**
                 * Displays image with appropriate count number on preview.
                 * @param {Number} index - Number of image that should be displayed.
                 */
                seek: function() {},

                /**
                 * Updates gallery with new set of options.
                 * @param {Object} configuration - Standart gallery configuration object.
                 * @param {Boolean} isInternal - Is this function called via breakpoints.
                 */
                // eslint-disable-next-line no-unused-vars
                updateOptions: function(configuration, isInternal) {},

                /**
                 * Updates gallery with specific set of items.
                 * @param {Array.<Object>} data - Set of gallery items to update.
                 */
                // eslint-disable-next-line no-unused-vars
                updateData: function(data) {},

                /**
                 * Returns current images list
                 *
                 * @returns {Array}
                 */
                returnCurrentImages: function() {},

                /**
                 * Updates gallery data partially by index
                 * @param {Number} index - Index of image in data array to be updated.
                 * @param {Object} item - Standart gallery image object.
                 *
                 */
                // eslint-disable-next-line no-unused-vars
                updateDataByIndex: function(index, item) {}
            };

            settings.$element.data('gallery', api);
            settings.api = settings.$element.data('gallery');
            settings.$element.trigger('gallery:loaded');
        }
    });
});
