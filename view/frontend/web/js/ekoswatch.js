'use strict';

// eslint-disable-next-line no-undef
define([
    'jquery',
    'Magento_Swatches/js/swatch-renderer'
], function($) {
    return function(_config, element) {
        const $widget = $(element);
        const widgetFullName = Object.entries($widget.data())
            .reduce((resolvedName, [key, val]) => {
                if (key === val?.widgetFullName) {
                    return key;
                }

                return resolvedName;
            }, '');

        // eslint-disable-next-line no-console
        console.log(`[ekoswatch] [init] widgetFullName: ${widgetFullName}`);

        function getSelectedProductId() {
            return $widget.data(widgetFullName)?.getProduct?.() ||
                $widget?.SwatchRenderer?.('getProduct'); // eslint-disable-line new-cap
        }

        function getSku(productId) {
            return $widget.data(widgetFullName)?.options?.jsonConfig?.sku[productId];
        }

        function onProductSelectionChange() {
            let productId = getSelectedProductId();

            // Bug fix - If no product selected (i.e. swatch option was deselected) set to null
            if (!productId ||
                // The getSelectedProductId() might return a jquery object if no product is selected.
                // In that case, we should ignore it - we only care about product ID (string or number).
                (
                    typeof productId !== 'string' &&
                    typeof productId !== 'number'
                )
            ) {
                productId = null;
            }

            // Get the SKU or null if swatch was deselected
            const productSku = productId ? getSku(productId) : null;

            // eslint-disable-next-line no-console
            console.log('[ekoswatch] [onProductSelectionChange] Currently selected product:', productId, productSku);

            // TODO - currently, the window.eko.gallery.setVariant function is only exposed on babybrezza, caraway, hellotushy and mockingbird galleries.
            // When we switch to generic gallery, we should make sure to expose this function.
            const setVariant = typeof window.eko?.gallery?.setVariant === 'function' ?
                window.eko.gallery.setVariant :
                () => {};

            const setActive = typeof window.eko?.gallery?.setActive === 'function' ?
                window.eko.gallery.setActive :
                () => {};

            setVariant(productSku);
            setActive('1');
        }

        // Listen for when all options are selected
        $widget.on('change.selectAllOptions', onProductSelectionChange);

        // Also initialize the selected product on load (to support deep linking to variant)
        onProductSelectionChange();
    };
});
