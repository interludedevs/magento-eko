<?php
namespace EkoLabs\Eko\Block;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Store\Model\StoreManagerInterface;

use EkoLabs\Eko\Api\LoggerInterface;
use EkoLabs\Eko\Api\EkoManagerInterface;
use EkoLabs\Eko\Api\LiquidInterface;

/**
 * GalleryBlock renders the gallery HTML
 */
class GalleryBlock extends Template implements BlockInterface
{
    private const LIQUID_TEMPLATE = 'eko-gallery';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var LiquidInterface
     */
    private $liquid;

    /**
     * @var EkoManagerInterface
     */
    private $ekoManager;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * GalleryBlock Constructor
     *
     * @param Template\Context $context
     * @param EkoManagerInterface $ekoManager
     * @param LiquidInterface $liquid
     * @param LoggerInterface $logger
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        EkoManagerInterface $ekoManager,
        LiquidInterface $liquid,
        LoggerInterface $logger,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->ekoManager = $ekoManager;
        $this->logger = $logger;
        $this->liquid = $liquid;
        $this->storeManager = $storeManager;
    }

    /**
     * Generates the HTML of the gallery
     *
     * @return string
     */
    protected function _toHtml()
    {
        $galleryObject = $this->ekoManager?->getGalleryObject();
        if (!$galleryObject) {
            return null;
        }

        $domain = $this->storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_WEB
        );

        // Create the render context
        $context = [
            'product' => $this->getLiquidProduct(),
            'shop' => [
                'domain' => $domain
            ]
        ];

        // Render the template
        return
            '<div class="gallery-placeholder" data-gallery-role="gallery-placeholder">' .
            $this->liquid->render(self::LIQUID_TEMPLATE, $context) .
            '</div>';
    }

    /**
     * Generates a product context strcuture for the liquid template from the Magento's product objects
     *
     * @return array
     */
    private function getLiquidProduct()
    {
        $product = $this->ekoManager->getProduct();

        $defaultVariant = null;
        $variants = [];
        $media = [];

        // if the product has variants their media is fetched and the default varinat is determined
        $simpleProducts = $this->ekoManager->getVariants($product);
        if ($simpleProducts && count($simpleProducts) > 0) {
            $defaultVariantProduct = $this->ekoManager->getDefaultVariant($product);
            foreach ($simpleProducts as $simpleProduct) {
                // get the media of the variant
                $variantMedia = $this->getProductMedia($simpleProduct);
                
                // the first image of the first variant is taken for the interactive
                if (count($media) === 0 && count($variantMedia) > 0) {
                    $media[] = $variantMedia[0];
                    array_shift($variantMedia);
                }

                $variant = [
                    'id' => $simpleProduct->getId(),
                    'sku' => $simpleProduct->getSku(),
                    'media' => $variantMedia
                ];
                $variants[] = $variant;

                if ($variant['id'] == $defaultVariantProduct->getId()) {
                    $defaultVariant = $variant;
                }
            }
        }

        // if the product is not configurable or if variants do not have media the product's media is used
        if (count($media) === 0) {
            $media = $this->getProductMedia($product);
        }

        $result = [
            'id' => $product->getId(),
            'sku' => $product->getSku(),
            'metafields' => [
                '_eko_' => [
                    'eko_gallery' => [
                        'value' => $this->ekoManager?->getGalleryObject()?->toJson()
                    ]
                ]
            ],
            'variants' => $variants,
            'media' => $media
        ];

        if ($defaultVariant) {
            $result['selected_or_first_available_variant'] = $defaultVariant;
        }

        return $result;
    }

    /**
     * Generates a media array of the given product
     *
     * @param Product $product
     * @return array
     */
    private function getProductMedia($product)
    {
        // TODO: handle videos
        $media = [];
        $this->logger->debug('GalleryBlock::getProductMedia --- getting media for ' . $product->getId());
        
        $magentoImages = $product->getMediaGalleryImages()?->getItems();
        if (is_array($magentoImages)) {
            usort($magentoImages, function ($el1, $el2) {
                return $el1['position'] <=> $el2['position'];
            });
        }
        foreach ($magentoImages as $magentoImage) {
            $this->logger->debug('GalleryBlock::getProductMedia --- got ' . $magentoImage->getFile());
            $media[] = [
                'id' => $magentoImage->getId(),
                'media_type' => 'image',
                'media' => $this->liquid->createMedia($magentoImage, $product)
            ];
        }

        return $media;
    }
}
