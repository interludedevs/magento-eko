<?php

namespace EkoLabs\Eko\Block;

use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use EkoLabs\Eko\Api\LoggerInterface;
use EkoLabs\Eko\Api\FilesystemInterface;
use EkoLabs\Eko\Api\EkoManagerInterface;
use EkoLabs\Eko\Api\VersionInterface;

/**
 * GalleryHeadBlock renders the gallery resources needed in the page head section
 */
class GalleryHeadBlock extends AbstractBlock
{
    private const SNIPPET_PATH = 'view/frontend/templates/liquid/snippets/eko-gallery-init-call.liquid';

    /**
     * @var EkoManagerInterface
     */
    private $ekoManager;

    /**
     * @var SecureHtmlRenderer
     */
    private $secureRenderer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Version
     */
    private $version;

    /**
     * @var FilesystemInterface
     */
    private $filesystem;

    /**
     * GalleryHeadBlock constructor.
     *
     * @param Context $context
     * @param EkoManagerInterface $ekoManager
     * @param SecureHtmlRenderer $secureRenderer
     * @param FilesystemInterface $filesystem
     * @param VersionInterface $version
     * @param LoggerInterface $logger
     * @param array $data
     */
    public function __construct(
        Context $context,
        EkoManagerInterface $ekoManager,
        SecureHtmlRenderer $secureRenderer,
        FilesystemInterface $filesystem,
        VersionInterface $version,
        LoggerInterface $logger,
        array $data = []
    ) {

        $this->ekoManager = $ekoManager;
        $this->secureRenderer = $secureRenderer;
        $this->filesystem = $filesystem;
        $this->version = $version;
        $this->logger = $logger;

        parent::__construct($context, $data);
        $this->logger->debug('GalleryHeadBlock::__construct: ', $data);
    }

    /**
     * Returns the eko gallery tags for the head section if applicable
     *
     * @return string|null
     */
    protected function _toHtml()
    {
        $result = null;
        if ($this->ekoManager->isEkofiedPage()) {
            $galleryObject = $this->ekoManager->getGalleryObject();
            $result =
                $this->getGalleryCss($galleryObject->getCssUrl()) .
                $this->getGalleryScript($galleryObject->getJsUrl()) .
                $this->getGalleryInitScript() .
                $this->getSwatchInitScript();
        }
        return $result;
    }

    /**
     * Get script tag which inits the gallery
     *
     * @return string
     */
    private function getGalleryInitScript()
    {
        $snippet = $this->filesystem->readFile(self::SNIPPET_PATH);
        if ($snippet) {
            $snippet = preg_replace('/<script>|<\/script>/i', '', $snippet);
        } else {
            $snippet = 'console.warn(\'failed loading '.self::SNIPPET_PATH.'\');';
        }

        $snippet .= sprintf(
            'window.eko = window.eko || {}; window.eko.magento = { name: \'%s\', version: \'%s\' };',
            $this->version->getName(),
            $this->version->getVersion()
        );
        
        return $this->secureRenderer->renderTag('script', [], $snippet, false);
    }

    /**
     * Get the gallery script tag
     *
     * @param string $url
     * @return string
     */
    private function getGalleryScript($url)
    {
        $attributes = [
            'id' => 'eko-gallery-script',
            'src' => $url,
            'type' => 'module',
            'async' => 'async',
            'data-container' => '.eko-gallery'
        ];
        return $this->secureRenderer->renderTag('script', $attributes, null, false);
    }

    /**
     * Get the gallert css tag
     *
     * @param string $url
     * @return string
     */
    private function getGalleryCss($url)
    {
        $attributes = [
            'rel' => 'stylesheet',
            'href' => $url
        ];
        return $this->secureRenderer->renderTag('link', $attributes, null, false);
    }

    /**
     * Get the swatch init script
     *
     * @return string
     */
    private function getSwatchInitScript()
    {
        return
            '<script type="text/x-magento-init">
            {
                "[data-role=swatch-options]": {
                    "EkoLabs_Eko/js/ekoswatch": {}
                },
                "[data-gallery-role=gallery-placeholder]": {
                    "EkoLabs_Eko/js/ekogallery": {}
                }
            }
            </script>;';
    }
}
