<?php

declare(strict_types=1);

namespace EkoLabs\Eko\Block;

use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\View\Element\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Directory\Model\Currency;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Sales\Model\Order;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\View\Helper\SecureHtmlRenderer;

use EkoLabs\Eko\Api\LoggerInterface;
use EkoLabs\Eko\Api\LiquidInterface;
use EkoLabs\Eko\Api\EkoManagerInterface;

/**
 * EABlock handles injection of EA script tag and webpixel init
 */
class EABlock extends AbstractBlock
{
    private const LIQUID_TEMPLATE = 'eko-ea-script-tag';

    /**
     * @var LiquidInterface
     */
    private $liquid;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var Configurable
     */
    private $configurable;

    /**
     * @var SecureHtmlRenderer
     */
    private $secureRenderer;

    /**
     * @var EkoManagerInterface
     */
    private $ekoManager;

    /**
     * @var array
     */
    private $arguments;

    /**
     * EABlock Constructor
     *
     * @param Context $context
     * @param LiquidInterface $liquid
     * @param StoreManagerInterface $storeManager
     * @param LoggerInterface $logger
     * @param ScopeConfigInterface $scopeConfig
     * @param Currency $currency
     * @param CheckoutSession $checkoutSession
     * @param SecureHtmlRenderer $secureRenderer
     * @param EkoManagerInterface $ekoManager
     * @param Configurable $configurable
     * @param array $data
     */
    public function __construct(
        Context $context,
        LiquidInterface $liquid,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger,
        ScopeConfigInterface $scopeConfig,
        Currency $currency,
        CheckoutSession $checkoutSession,
        SecureHtmlRenderer $secureRenderer,
        EkoManagerInterface $ekoManager,
        Configurable $configurable,
        array $data = []
    ) {
        $this->liquid = $liquid;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->currency = $currency;
        $this->checkoutSession = $checkoutSession;
        $this->configurable = $configurable;
        $this->secureRenderer = $secureRenderer;
        $this->ekoManager = $ekoManager;
        $this->arguments = $data;

        parent::__construct($context, $data);
        $this->logger->debug('EABlock::__construct: ', $data);
    }

    /**
     * Generates the HTML script tags of the EA init
     *
     * @return string
     */
    protected function _toHtml(): string
    {
        $ekoEnv = $this->scopeConfig->getValue(
            'eko_gallery/advanced/eko_env',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (!$ekoEnv || $ekoEnv === '') {
            $ekoEnv = 'production';
        }

        $debugLog = $this->scopeConfig->getValue(
            'eko_gallery/advanced/ad_debug_log',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $ekoGalleryBlockName = $this->scopeConfig->getValue(
            'eko_gallery/auto_inject/ai_gallery_block',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $ekoGalleryBlock = $this->getLayout()->getBlock($ekoGalleryBlockName);

        $domain = $this->storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_WEB
        );

        $cartCurrency = $this->getCartCurrency();

        $event = ($this->arguments['event'] ?? null);

        $order = $this->getOrder();

        $product = $this->getProduct();

        $loadEA = !$ekoGalleryBlock;

        $productLinkoutSelector = null;
        if ($this->ekoManager->isEkofiedPage()) {
            $galleryObj = $this->ekoManager->getGalleryObject()->toJson();
            $productLinkoutSelector = $galleryObj['options']['productLinkoutSelector'] ?? null;
        }

        $pixelConfig = json_encode([
            'loadEA' => $loadEA,
            'event' => $event,
            'order' => $order,
            'product' => $product,
            'debugLog' => $debugLog,
            'productLinkoutSelector' => $productLinkoutSelector,
            'currency' => [
                'code' => $cartCurrency->getCode(),
                'symbol' => $cartCurrency->getCurrencySymbol()
            ]
        ]);

        $luda = $this->liquid->render(self::LIQUID_TEMPLATE, [
            'galleryEnv' => $ekoEnv,
            'loadScript' => $loadEA,
            'shop' => [
                'domain' => $domain
            ]
        ]);
        $luda = preg_replace('/<script>|<\/script>/i', '', $luda);
        $ludaScriptTag = $this->secureRenderer->renderTag('script', [], $luda, false);

        return
            $ludaScriptTag .
            "<script type='text/x-magento-init'>
                {
                    \"*\": {
                        \"ekowebpixel\": ".$pixelConfig."
                    }
                }
            </script>";
    }

    /**
     * Get the currency properties of the store
     *
     * @return Magento\Directory\Model\Currency;
     */
    private function getCartCurrency()
    {
        $store = $this->storeManager->getStore();
        $currencyCode = $store->getCurrentCurrencyCode();
        return $this->currency->load($currencyCode);
    }

    /**
     * Get the current product if applicable
     *
     * @return array
     */
    private function getProduct()
    {
        $result = null;
        $product = $this->ekoManager->getProduct();
        if ($product) {
            $result = [
                'product_id' => $product->getId(),
                'product_name' => $product->getName(),
                'product_sku' => $product->getSku(),
                'product_price_value' => $product->getPrice()
            ];
        }
        return $result;
    }

    /**
     * Get the last real order
     *
     * @return array
     */
    private function getOrder()
    {
        $result = null;
        
        $quote = $this->checkoutSession->getQuote();
        $hasActiveQuote = ($quote && $quote->getId());
        $order = $this->checkoutSession->getLastRealOrder();
        // the last order is returned only in cases that the cart is empty
        if (!$hasActiveQuote && $order && $order->getId()) {
            $result = [
                'order_id' => $order->getRealOrderId(),
                'checkout_id' => $order->getQuoteId(),
                'subtotalAmount' => $order->getSubtotal(),
                'summary_count' => count($order->getAllVisibleItems()),
                'items' => []
            ];

            foreach ($order->getAllVisibleItems() as $item) {
                $productId = $item->getProductId();
                // Check if this is a simple product that's part of a configurable product
                $parentIds = $this->configurable->getParentIdsByChild($productId);
                if (!empty($parentIds)) {
                    $productId = reset($parentIds);  // Get the first parent ID
                }
                $this->logger->debug(
                    'EABlock::getOrder --- itemId: ' . $item->getProductId() . '; productId: ' . $productId,
                    $parentIds
                );
                $result['items'][] = [
                    'product_id' => $productId,
                    'product_sku' => $item->getSku(),
                    'product_name' => $item->getName(),
                    'qty' => (int)$item->getQtyOrdered(),
                    'product_price_value' => $item->getPrice()
                ];
            }
        }
        return $result;
    }
}
