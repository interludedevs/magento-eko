<?php
namespace EkoLabs\Eko\Block\Adminhtml\System\Config;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use EkoLabs\Eko\Api\VersionInterface;

/**
 * VersionInfo template renders version info in the admin settings
 */
class VersionInfo extends Template implements RendererInterface
{
    /**
     * @var Version
     */
    private $version;

    /**
     * Constructor
     * @param Template\Context $context
     * @param VersionInterface $version
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        VersionInterface $version,
        array $data = []
    ) {
        $this->version = $version;
        parent::__construct($context, $data);
    }

    /**
     * Renders the version information
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = '<tr id="row_' . $element->getHtmlId() . '">';
        $html .= '<td class="label" colspan="5" style="text-align: left;">';
        $html .= '<div style="padding:10px;background-color:#f8f8f8;border:1px solid #ddd;margin-top:7px;">';
        $html .= '<strong>' . $this->version->getName() . '</strong>';
        $html .= ' (' . $this->version->getVersion() . ')';
        $html .= '</div>';
        $html .= '</td>';
        $html .= '</tr>';

        return $html;
    }
}
