<?php

namespace EkoLabs\Eko\Block;

use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use EkoLabs\Eko\Api\LoggerInterface;
use EkoLabs\Eko\Api\FilesystemInterface;
use EkoLabs\Eko\Api\EkoManagerInterface;

/**
 * TrafficAllocationBlock handles injecting traffic allocation snippets to the page head
 */
class TrafficAllocationBlock extends AbstractBlock
{
    private const DECIDER_PATH = 'view/frontend/templates/snippets/eko-traffic-allocation-decider.txt';
    private const APPLIER_PATH = 'view/frontend/templates/snippets/eko-traffic-allocation-applier.txt';

    /**
     * @var EkoManagerInterface
     */
    private $ekoManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SecureHtmlRenderer
     */
    private $secureRenderer;

    /**
     * @var string
     */
    private $decider;

    /**
     * @var string
     */
    private $applier;

    /**
     * SplitTestBlock constructor
     *
     * @param Context $context
     * @param EkoManagerInterface $ekoManager
     * @param FilesystemInterface $filesystem
     * @param SecureHtmlRenderer $secureRenderer
     * @param LoggerInterface $logger
     * @param array $data
     */
    public function __construct(
        Context $context,
        EkoManagerInterface $ekoManager,
        FilesystemInterface $filesystem,
        SecureHtmlRenderer $secureRenderer,
        LoggerInterface $logger,
        array $data = []
    ) {

        $this->ekoManager = $ekoManager;
        $this->secureRenderer = $secureRenderer;
        $this->logger = $logger;

        $this->decider = $this->loadSnippet($filesystem, self::DECIDER_PATH);
        $this->applier = $this->loadSnippet($filesystem, self::APPLIER_PATH);
        
        parent::__construct($context, $data);
        $this->logger->debug('TrafficAllocationBlock::__construct: ', $data);
    }

    /**
     * Generates the HTML script tag of the split test if applicable
     *
     * @return string|null
     */
    protected function _toHtml()
    {
        $result = null;
        $trafficAllocationSettings = $this->getTrafficAllocationSettings();
        if ($trafficAllocationSettings && $trafficAllocationSettings['active']) {
            $this->logger->debug('TrafficAllocationBlock::_toHtml: injecting decider snippet');

            $deciderScript = '
                window.eko = window.eko || {};
                window.eko.trafficAllocation = {
                    trafficAllocationConfig: ' . json_encode($trafficAllocationSettings) .
                '};' .
                $this->decider .
                'const decision = window.eko?.trafficAllocation?.allocateTraffic();
            ';
            $result = $this->secureRenderer->renderTag('script', [], $deciderScript, false);

            $config = $this->ekoManager?->getConfig();
            $queryParamKey = $config['queryParamKey'];
            $queryParamValue = $config['queryParamValue'];
            
            if ($trafficAllocationSettings['apply_method'] === 'redirect' &&
                $this->ekoManager?->isConditional() &&
                $this->ekoManager?->getProduct() &&
                $this->ekoManager?->getGalleryObject()
            ) {
                $this->logger->debug('TrafficAllocationBlock::_toHtml: injecting applier snippet');
                
                $variants = [];
                if (!$this->ekoManager?->isEkofiedPage()) {
                    $variants = [
                        'eko' => [
                            'redirectUrl' => [
                                'searchParams' => [
                                    $queryParamKey => $queryParamValue
                                ]
                            ]
                        ]
                    ];
                }

                $applierScript = '
                    window.eko = window.eko || {};
                    window.eko.trafficAllocation = window.eko.trafficAllocation || {};
                    window.eko.trafficAllocation.variants = ' . json_encode($variants) . ';' .
                    $this->applier .
                    'const splitResult = window.eko?.trafficAllocation?.applyVariant();
                ';

                $result .= $this->secureRenderer->renderTag('script', [], $applierScript, false);
            }
        }
        return $result;
    }

    /**
     * Gets the straffic allocation seetings
     *
     * @return array|null
     */
    private function getTrafficAllocationSettings()
    {
        $result = null;
        $platformSettings = $this->ekoManager->getPlatformSettings();
        if (isset($platformSettings['trafficAllocation'])) {
            $result = $platformSettings['trafficAllocation'];
        }
        return $result;
    }

    /**
     * Loads the traffic allocation snippet from the file system
     *
     * @param FileSystem $filesystem
     * @param string $snippet
     * @return string
     */
    private function loadSnippet($filesystem, $snippet)
    {
        $result = $filesystem->readFile($snippet);
        if (!$result) {
            $result = 'console.log(\'failed loading split test snippet from '.$snippet.'\');';
        }
        return $result;
    }
}
