<?php

namespace EkoLabs\Eko\Api;

/**
 * @api
 */
interface LiquidInterface
{
    /**
     * Render a liquid template with the provided context object
     *
     * @param string $template
     * @param array $context
     * @return string
     */
    public function render($template, $context);

    /**
     * Create a liquid media object for a magento image of a product
     *
     * @param Image $mediaGalleryImage
     * @param Product $product
     * @return Media
     */
    public function createMedia($mediaGalleryImage, $product);
}
