<?php
namespace EkoLabs\Eko\Api;

interface PlatformSettingsManagerInterface
{
    /**
     * Get custom setting for a store
     *
     * @param string $storeCode
     * @return string
     */
    public function getSettings($storeCode = null);

    /**
     * Set custom setting for a store
     *
     * @param string $value
     * @param string $storeCode
     * @return bool
     */
    public function setSettings($value, $storeCode = null);
}
