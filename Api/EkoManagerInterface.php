<?php

declare(strict_types=1);

namespace EkoLabs\Eko\Api;

use EkoLabs\Eko\Api\Data\GalleryObjectInterface;

/**
 * @api
 */
interface EkoManagerInterface
{
    /**
     * Get the eko gallery object for the current product if applicable
     *
     * @return GalleryObjectInterface|null
     */
    public function getGalleryObject(): ?GalleryObjectInterface;

    /**
     * Get the current product object
     *
     * @return \Magento\Catalog\Model\Product|null
     */
    public function getProduct();

    /**
     * Try to estimate which is the default variant of a product
     *
     * @param Product $product
     * @return Product|null
     */
    public function getDefaultVariant($product);

    /**
     * Gets the simple products variants of a configurable product
     *
     * @param Product $product
     * @return array|null
     */
    public function getVariants($product);

    /**
     * Get the module's config from the admin settings
     *
     * @return array
     */
    public function getConfig(): array;

    /**
     * Is the current page should include an eko content
     *
     * @return boolean
     */
    public function isEkofiedPage(): bool;

    /**
     * Is the auto inject conditional
     *
     * @return boolean
     */
    public function isConditional(): bool;

    /**
     * In case of a conditional auto inject - is the condition met?
     *
     * @return boolean
     */
    public function isConditionMet(): bool;
}
