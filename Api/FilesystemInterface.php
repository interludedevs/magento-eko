<?php

namespace EkoLabs\Eko\Api;

/**
 * @api
 */
interface FilesystemInterface
{
    /**
     * Read a file from the module's filesystem
     *
     * @param string $filename
     * @return string|null
     */
    public function readFile($filename);

    /**
     * Test is a file from the module's filesystem exists and readbale
     *
     * @param string $filename
     * @return boolean
     */
    public function isExists($filename);

    /**
     * Returns parent directory's path
     *
     * @param string $path
     * @return string
     */
    public function getParentDirectory($path);

    /**
     * Get the basename of a path
     *
     * @param string $path
     * @return string
     */
    public function getBasename($path);
}
