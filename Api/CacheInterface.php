<?php

declare(strict_types=1);

namespace EkoLabs\Eko\Api;

/**
 * @api
 */
interface CacheInterface
{
    /**
     * Invalidate eko's blocks cache for the product id
     *
     * @param string $productId
     * @return void
     */
    public function invalidate($productId);

    /**
     * Invalidate cache entries with the eko tag
     *
     * @return void
     */
    public function invalidateAll();

    /**
     * Modify the cache identifier for conditional injection pages to allow two copies in the cache
     *
     * @param string $identifier
     * @return string
     */
    public function getIdentifier($identifier);

    /**
     * Add the eko tag if the current product has an eko gallery onject
     *
     * @param array $tags
     * @return array
     */
    public function getTags(array $tags = []);
}
