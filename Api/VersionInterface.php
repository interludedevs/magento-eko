<?php

namespace EkoLabs\Eko\Api;

/**
 * @api
 */
interface VersionInterface
{
    /**
     * Get the module's name
     *
     * @return string
     */
    public function getName();

    /**
     * Get the module's version
     *
     * @return string
     */
    public function getVersion();
}
