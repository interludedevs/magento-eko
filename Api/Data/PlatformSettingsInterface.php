<?php
declare(strict_types=1);

namespace EkoLabs\Eko\Api\Data;

interface PlatformSettingsInterface
{
    /**
     * Get the value of the id column
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Get the value of the store_id column
     *
     * @return int
     */
    public function getStoreId(): string;

    /**
     * Get the value of the setting_value column
     *
     * @return string
     */
    public function getValue(): string;

    /**
     * Set the value of the store_id column
     *
     * @param string $storeId
     * @return PlatformSettingsInterface
     */
    public function setStoreId(string $storeId): PlatformSettingsInterface;

    /**
     * Set the value of the setting_value column
     *
     * @param string $value
     * @return PlatformSettingsInterface
     */
    public function setValue(string $value): PlatformSettingsInterface;
}
