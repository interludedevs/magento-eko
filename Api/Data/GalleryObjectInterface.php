<?php

declare(strict_types=1);

namespace EkoLabs\Eko\Api\Data;

/**
 * @api
 */
interface GalleryObjectInterface
{
    /**
     * Get the gallery object
     *
     * @return array|null
     */
    public function toJson() : ?array;

    /**
     * Check if the gallery has an experiment configuration
     *
     * @param string|null $type
     * @return boolean
     */
    public function hasActiveExperiment(string $type = null) : bool;

    /**
     * Get the experiment configuration if applicable
     *
     * @param string|null $type
     * @return array|null
     */
    public function getActiveExperiment(string $type = null) : ?array;

    /**
     * Get the gallery CSS URL
     *
     * @return string
     */
    public function getCssUrl() : string;

    /**
     * Get the gallery JS URL
     *
     * @return string
     */
    public function getJsUrl() : string;
}
