<?php

declare(strict_types=1);

namespace EkoLabs\Eko\Api\Data;

/**
 * @api
 */
interface ProductAttributesInterface
{
    /**
     * Get the value of the id column
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Get the value of the product_id column
     *
     * @return int
     */
    public function getProductId(): int;

    /**
     * Get the value of the eko_gallery_obj column
     *
     * @return string
     */
    public function getEkoGalleryObject(): string;

    /**
     * Set the value of the product_id column
     *
     * @param int $value
     * @return ProductAttributesInterface
     */
    public function setProductId(int $value): ProductAttributesInterface;

    /**
     * Set the value of the eko_gallery_obj column
     *
     * @param string $value
     * @return ProductAttributesInterface
     */
    public function setEkoGalleryObject(string $value): ProductAttributesInterface;
}
