<?php

namespace EkoLabs\Eko\Api;

/**
 * @api
 */
interface LoggerInterface
{
    /**
     * Send debug message with an optional context array to the logger if logging is enabled
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function debug($message, array $context = []);
}
