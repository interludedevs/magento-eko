<?php
namespace EkoLabs\Eko\Api;

interface QuoteIdManagementInterface
{
    /**
     * Get current quote ID
     *
     * @return string|null
     */
    public function getQuoteId();
}
