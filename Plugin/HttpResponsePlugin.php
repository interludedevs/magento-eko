<?php
namespace EkoLabs\Eko\Plugin;

use Magento\Framework\App\Response\Http;
use EkoLabs\Eko\Api\LoggerInterface;
use EkoLabs\Eko\Api\EkoManagerInterface;

class HttpResponsePlugin
{
    /**
     * @var EkoManagerInterface
     */
    private $ekoManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param EkoManagerInterface $ekoManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        EkoManagerInterface $ekoManager,
        LoggerInterface $logger
    ) {
        $this->ekoManager = $ekoManager;
        $this->logger = $logger;
    }

    /**
     * Add the eko gallery js and css to the Link http header of the response
     *
     * @param Http $subject
     * @return void
     */
    public function beforeSendResponse(Http $subject)
    {
        if ($this->ekoManager->isEkofiedPage()) {
            $galleryObj = $this->ekoManager->getGalleryObject();
            $ekoPreloads = sprintf(
                '<%s>; as="style"; rel="preload", <%s>; as="script"; rel="modulepreload"',
                $galleryObj->getCssUrl(),
                $galleryObj->getJsUrl()
            );

            $header = $subject->getHeader('Link');
            if ($header) {
                $header .= ',' . $ekoPreloads;
            } else {
                $header = $ekoPreloads;
            }

            $subject->setHeader('Link', $header);
        }
        return $subject;
    }
}
