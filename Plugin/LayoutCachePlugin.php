<?php
namespace EkoLabs\Eko\Plugin;

use Magento\Framework\App\Cache\Type\Layout;
use EkoLabs\Eko\Api\LoggerInterface;
use EkoLabs\Eko\Api\CacheInterface;

class LayoutCachePlugin
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * LayoutCachePlugin constructor
     *
     * @param CacheInterface $cache
     * @param LoggerInterface $logger
     */
    public function __construct(
        CacheInterface $cache,
        LoggerInterface $logger
    ) {
        $this->cache = $cache;
        $this->logger = $logger;
    }

    /**
     * Wrap the cache load functionality - modify the identifier if needed
     *
     * @param Layout $subject
     * @param callable $proceed
     * @param string $identifier
     * @return void
     */
    public function aroundLoad(Layout $subject, callable $proceed, $identifier)
    {
        $identifier = $this->cache->getIdentifier($identifier);
        $result = $proceed($identifier);
        $this->logger->debug(
            "LayoutCachePlugin::aroundLoad - Identifier: $identifier, Hit: " . ($result !== false ? 'Yes' : 'No')
        );
        return $result;
    }

    /**
     * UWrap the cache save functionality - modify the identifier if needed
     *
     * @param Layout $subject
     * @param callable $proceed
     * @param [type] $data
     * @param [type] $identifier
     * @param array $tags
     * @param [type] $lifeTime
     * @return void
     */
    public function aroundSave(
        Layout $subject,
        callable $proceed,
        $data,
        $identifier,
        array $tags = [],
        $lifeTime = null
    ) {
        $identifier = $this->cache->getIdentifier($identifier);
        $tags = $this->cache->getTags($tags);
        $this->logger->debug("LayoutCachePlugin::aroundSave - Identifier: $identifier, Tags: " . implode(', ', $tags));
        return $proceed($data, $identifier, $tags, $lifeTime);
    }
}
