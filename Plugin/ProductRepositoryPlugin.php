<?php
declare(strict_types=1);

namespace EkoLabs\Eko\Plugin;

use EkoLabs\Eko\Model\ProductAttributesFactory;
use EkoLabs\Eko\Model\ProductAttributesResource;
use EkoLabs\Eko\Api\Data\ProductAttributesInterface;
use EkoLabs\Eko\Api\LoggerInterface;
use EkoLabs\Eko\Api\CacheInterface;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductSearchResultInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

use Magento\Framework\App\ResourceConnection;

class ProductRepositoryPlugin
{
    /**
     * @var ProductAttributesResource
     */
    private $resource;

    /**
     * @var ProductAttributesFactory
     */
    private $factory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var CacheInterface
     */
    private $cache;

     /**
      * ProductRepositoryPlugin constructor.
      *
      * @param ProductAttributesResource $resource
      * @param ProductAttributesFactory $attributesFactory
      * @param LoggerInterface $logger
      * @param ResourceConnection $resourceConnection
      * @param CacheInterface $cache
      */
    public function __construct(
        ProductAttributesResource $resource,
        ProductAttributesFactory $attributesFactory,
        LoggerInterface $logger,
        ResourceConnection $resourceConnection,
        CacheInterface $cache
    ) {
        $this->resource = $resource;
        $this->factory = $attributesFactory;
        $this->logger = $logger;
        $this->resourceConnection = $resourceConnection;
        $this->cache = $cache;
    }

    /**
     * Since the join functionality in extenstion_attributes.xml works
     * only for product search we need to do it manually for
     * GET /rest/V1/products/{sku}
     *
     * @param ProductRepositoryInterface $subject
     * @param ProductInterface $product
     * @return ProductInterface
     */
    public function afterGet(
        ProductRepositoryInterface $subject,
        ProductInterface $product
    ): ProductInterface {
        $this->logger->debug('ProductRepositoryPlugin::afterGet');
        return $this->setProductExtensionAttributes($product);
    }

    /**
     * Since the join functionality in extenstion_attributes.xml works
     * only for product search we need to do it manually for
     * GET /rest/V!/products/{sku}
     *
     * @param ProductRepositoryInterface $subject
     * @param ProductInterface $product
     * @return ProductInterface
     */
    public function afterGetById(
        ProductRepositoryInterface $subject,
        ProductInterface $product
    ): ProductInterface {
        
        $this->logger->debug('ProductRepositoryPlugin::afterGetById');
        return $this->setProductExtensionAttributes($product);
    }

    /**
     * Save the extension attribute to eko_product_attributes table
     *
     * @param ProductRepositoryInterface $subject
     * @param callable $proceed
     * @param ProductInterface $product
     * @param bool $saveOptions
     * @return ProductInterface
     */
    public function aroundSave(
        ProductRepositoryInterface $subject,
        callable $proceed,
        ProductInterface $product,
        $saveOptions = false
    ) {
        // Get the gallery from the extension attribute
        $extensionAttributes = $product->getExtensionAttributes();
        $gallery = $extensionAttributes->getEkoGallery();
        
        // Call the original save method
        $result = $proceed($product, $saveOptions);

        // save the gallery to the eko's product attributes table
        $productAttributes = $this->getProductAttributes((int) $product->getId());
        if ($gallery && $gallery !== '{}') {
            $this->logger->debug(
                'ProductRepositoryPlugin::aroundSave gallery' . json_encode($gallery, JSON_UNESCAPED_UNICODE)
            );
            if (!$productAttributes || !$productAttributes->getProductId()) {
                $this->logger->debug('ProductRepositoryPlugin::aroundSave - creating a new model');
                $productAttributes = $this->createProductAttributes();
                $productAttributes->setProductId((int) $product->getId());
                $productAttributes->setEkoGalleryObject($gallery);
                $this->insertProductAttributes($productAttributes);
            } else {
                $this->logger->debug('ProductRepositoryPlugin::aroundSave - updating model');
                $productAttributes->setEkoGalleryObject($gallery);
                $productAttributes->save();
            }

            $this->logger->debug(
                'ProductRepositoryPlugin::aroundSave - after save:' .
                $productAttributes->getProductId() . '; ' .
                $productAttributes->getEkoGalleryObject()
            );
        } else {
            $this->logger->debug('ProductRepositoryPlugin::aroundSave - deleting model');
            $productAttributes->delete();
        }

        // invalidate cache
        $this->cache->invalidate($product->getId());

        return $result;
    }

    /**
     * Set the eko attributes on the product object as extension attributes
     *
     * @param ProductInterface $product
     * @return ProductInterface
     */
    private function setProductExtensionAttributes(
        ProductInterface $product
    ): ProductInterface {
        
        $productAttributes = $this->getProductAttributes((int) $product->getId());
        $extensionAttributes = $product->getExtensionAttributes();
        $ekoGallery = $productAttributes->getEkoGalleryObject();
        $extensionAttributes->setEkoGallery($ekoGallery);
        $product->setExtensionAttributes($extensionAttributes);

        return $product;
    }

    /**
     * Insert eko attributes to the database
     *
     * @param ProductAttributesInterface $productAttributes
     * @return void
     */
    private function insertProductAttributes(ProductAttributesInterface $productAttributes)
    {
        $this->logger->debug(
            'ProductRepositoryPlugin::insertProductAttributes:' .
            $productAttributes->getProductId() . '; ' .  $productAttributes->getEkoGalleryObject()
        );

        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('eko_product_attributes');

        $data = [
            'product_id' => $productAttributes->getProductId(),
            'eko_gallery_obj' => $productAttributes->getEkoGalleryObject(),
        ];

        $connection->insert($tableName, $data);
    }

    /**
     * Get eko attributes from the database
     *
     * @param int $productId
     * @return ProductAttributesInterface
     */
    public function getProductAttributes(int $productId): ProductAttributesInterface
    {
        $object = $this->createProductAttributes();
        $this->resource->load($object, $productId, 'product_id');

        return $object;
    }

    /**
     * Create empty product attributes object
     *
     * @return ProductAttributesInterface
     */
    public function createProductAttributes(): ProductAttributesInterface
    {
        return $this->factory->create();
    }
}
