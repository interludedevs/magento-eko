<?php

namespace EkoLabs\Eko\Plugin;

use Magento\Framework\View\Layout;
use EkoLabs\Eko\Api\EkoManagerInterface;
use EkoLabs\Eko\Api\LoggerInterface;

class LayoutPlugin
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EkoManagerInterface
     */
    private $ekoManager;

    /**
     * LayoutPlugin constructor
     *
     * @param EkoManagerInterface $ekoManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        EkoManagerInterface $ekoManager,
        LoggerInterface $logger
    ) {
        $this->ekoManager = $ekoManager;
        $this->logger = $logger;
    }

    /**
     * Modify the layout to include the eko gallery where applicable
     *
     * @param Layout $subject
     * @param LayoutUpdate $layoutUpdate
     * @return void
     */
    public function afterGetUpdate(Layout $subject, $layoutUpdate)
    {
        if ($this->ekoManager->isEkofiedPage()) {
            $config = $this->ekoManager->getConfig();
            $autoInjectContainer = $config['autoInjectContainer'];
            $ekoGalleryBlockName = $config['ekoGalleryBlockName'];
            $autoInjectReplaceBlock = $config['autoInjectReplaceBlock'];

            $this->logger->debug('LayoutPlugin::afterGetUpdate - adding eko gallery block to ' . $autoInjectContainer);

            $layoutUpdate->addUpdate(sprintf(
                '<body>
                    <referenceContainer name="%s">
                        <block class="EkoLabs\Eko\Block\GalleryBlock" name="%s" />
                    </referenceContainer>
                </body>',
                $autoInjectContainer,
                $ekoGalleryBlockName
            ));

            if ($autoInjectReplaceBlock) {
                $this->logger->debug('LayoutPlugin::afterGetUpdate - removing ' . $autoInjectReplaceBlock);
                $layoutUpdate->addUpdate(sprintf(
                    '<body>
                        <referenceBlock name="%s" display="false" />
                    </body>',
                    $autoInjectReplaceBlock
                ));
            }
        }

        return $layoutUpdate;
    }
}
