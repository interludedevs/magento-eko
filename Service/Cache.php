<?php

declare(strict_types=1);

namespace EkoLabs\Eko\Service;

use EkoLabs\Eko\Api\CacheInterface;
use EkoLabs\Eko\Api\LoggerInterface;
use EkoLabs\Eko\Api\EkoManagerInterface;

use Magento\Framework\App\CacheInterface as MagentoCache;
use Magento\Framework\EntityManager\EventManager;
use Magento\Framework\Indexer\CacheContextFactory;

/**
 * @api
 */
class Cache implements CacheInterface
{
    private const CACHE_TAG = 'ekofied_cache_tag';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MagentoCache
     */
    private $cache;

    /**
     * @var CacheContextFactory
     */
    private $cacheContextFactory;

    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var EkoManagerInterface
     */
    private $ekoManager;

    /**
     * Cache constructor
     *
     * @param MagentoCache $cache
     * @param CacheContextFactory $cacheContextFactory
     * @param EventManager $eventManager
     * @param EkoManagerInterface $ekoManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        MagentoCache $cache,
        CacheContextFactory $cacheContextFactory,
        EventManager $eventManager,
        EkoManagerInterface $ekoManager,
        LoggerInterface $logger
    ) {
        $this->cache = $cache;
        $this->cacheContextFactory = $cacheContextFactory;
        $this->eventManager = $eventManager;
        $this->ekoManager = $ekoManager;
        $this->logger = $logger;
    }

    /**
     * Invalidate eko's blocks cache for the product id
     *
     * @param string $productId
     * @return void
     */
    public function invalidate($productId)
    {
        $this->logger->debug('Cache::invalidate --- invalidating ' . $productId);

        if ($productId) {
            $cacheContext = $this->cacheContextFactory->create();
            $cacheContext->registerEntities(\Magento\Catalog\Model\Product::CACHE_TAG, [$productId]);
            // clean the magento cache entities related to the product id
            $this->cache->clean($cacheContext->getIdentities());
            // dispatch clean_cache_by_tags for other modules (e.g. CDNs) to handle purging
            $this->eventManager->dispatch('clean_cache_by_tags', ['object' => $cacheContext]);
        }
    }

    /**
     * Invalidate cache entries with the eko tag
     *
     * @return void
     */
    public function invalidateAll()
    {
        $this->logger->debug('Cache::invalidateAll --- invalidating all entries with tag ' . self::CACHE_TAG);
        $cacheContext = $this->cacheContextFactory->create();
        $cacheContext->registerTags([self::CACHE_TAG]);
        // clean the magento cache entities related to the product id
        $this->cache->clean($cacheContext->getIdentities());
        // dispatch clean_cache_by_tags for other modules (e.g. CDNs) to handle purging
        $this->eventManager->dispatch('clean_cache_by_tags', ['object' => $cacheContext]);
    }

    /**
     * Modify the cache identifier for conditional injection pages to allow two copies in the cache
     *
     * @param string $identifier
     * @return string
     */
    public function getIdentifier($identifier)
    {
        if ($this->ekoManager->isConditional() &&
            $this->ekoManager->isConditionMet()
        ) {
            $config = $this->ekoManager->getConfig();
            $queryParamKey = $config['queryParamKey'];
            $queryParamValue = $config['queryParamValue'];

            $ekoIdentifier = 'eko:' . $queryParamKey . '=' . $queryParamValue;
            $identifier .= $ekoIdentifier;
            $this->logger->debug('Cache::getIdentifier --- modifying identifier ' . $identifier);
        }

        return $identifier;
    }

    /**
     * Add the eko tag if the current product has an eko gallery onject
     *
     * @param array $tags
     * @return array
     */
    public function getTags(array $tags = [])
    {
        // add cache tags for all entities related to products with
        // a gallery (i.e. that eko context was published to)
        // those include ekofied pages and non ekofied page
        if ($this->ekoManager?->getGalleryObject()) {
            $tags[]=self::CACHE_TAG;
            $this->logger->debug('Cache::getTags --- adding eko tag ' . self::CACHE_TAG, $tags);
        }
        return $tags;
    }
}
