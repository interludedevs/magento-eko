<?php
namespace EkoLabs\Eko\Service;

use EkoLabs\Eko\Model\PlatformSettingsFactory;
use EkoLabs\Eko\Model\PlatformSettingsResource;
use EkoLabs\Eko\Api\Data\PlatformSettingsInterface;
use EkoLabs\Eko\Api\PlatformSettingsManagerInterface;
use EkoLabs\Eko\Api\LoggerInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\StoreManagerInterface;

class PlatformSettingsManager implements PlatformSettingsManagerInterface
{
    /**
     * @var PlatformSettingsResource
     */
    private $resource;

    /**
     * @var PlatformSettingsFactory
     */
    private $factory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     *  PlatformSettingsManager constructor
     *
     * @param PlatformSettingsResource $resource
     * @param PlatformSettingsFactory $attributesFactory
     * @param LoggerInterface $logger
     * @param ResourceConnection $resourceConnection
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        PlatformSettingsResource $resource,
        PlatformSettingsFactory $attributesFactory,
        LoggerInterface $logger,
        ResourceConnection $resourceConnection,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->factory = $attributesFactory;
        $this->logger = $logger;
        $this->resourceConnection = $resourceConnection;
        $this->storeManager = $storeManager;
    }

    /**
     * Get custom setting for a store
     *
     * @param string $storeCode
     * @return string
     */
    public function getSettings($storeCode = null)
    {
        $this->logger->debug('PlatformSettingsManager::getSettings - ' . ' storeCode: ' . $storeCode);
        $result = null;
        try {
            $store = $this->getStore($storeCode);
            if ($store) {
                $storeId = $store->getId();
                $platformSettings = $this->load($storeId);
                $result = $platformSettings->getId() ? $platformSettings->getValue() : null;
            }
        } catch (Exception $e) {
            $result = null;
        }
        return $result;
    }

    /**
     * Set custom setting for a store
     *
     * @param string $value
     * @param string $storeCode
     * @return bool
     */
    public function setSettings($value, $storeCode = null)
    {
        $this->logger->debug('PlatformSettingsManager::setSettings - ' . $value . ' storeCode: ' . $storeCode);
        $result = false;
        try {
            $store = $this->getStore($storeCode);
            if ($store) {
                $storeId = $store->getId();
                $platformSettings = $this->load($storeId);
                $platformSettings->setValue($value);
                if ($platformSettings->getId()) {
                    $this->update($platformSettings);
                } else {
                    $platformSettings->setStoreId($storeId);
                    $this->insert($platformSettings);
                }
                $result = true;
            }
        } catch (Exception $e) {
            $result = false;
        }
        
        return $result;
    }

    /**
     * Update existing platform settings to the database
     *
     * @param PlatformSettingsInterface $platformSettings
     * @return void
     */
    private function update(PlatformSettingsInterface $platformSettings)
    {
        $this->logger->debug(
            'PlatformSettingsManager::save:' .
            $platformSettings->getStoreId() . '; ' .  $platformSettings->getValue()
        );

        $platformSettings->save();
    }

    /**
     * Insert platform settings to the database
     *
     * @param PlatformSettingsInterface $platformSettings
     * @return void
     */
    private function insert(PlatformSettingsInterface $platformSettings)
    {
        $this->logger->debug(
            'PlatformSettingsManager::insert:' .
            $platformSettings->getStoreId() . '; ' .  $platformSettings->getValue()
        );

        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('eko_platform_settings');

        $data = [
            'store_id' => $platformSettings->getStoreId(),
            'setting_value' => $platformSettings->getValue(),
        ];

        $connection->insert($tableName, $data);
    }

    /**
     * Get platform settings from the database
     *
     * @param string $storeId
     * @return PlatformSettingsInterface
     */
    private function load(string $storeId): PlatformSettingsInterface
    {
        $this->logger->debug('PlatformSettingsManager::load:' . $storeId);

        $platformSettings = $this->factory->create();
        $this->resource->load($platformSettings, $storeId, 'store_id');

        return $platformSettings;
    }

    /**
     * Get store - either once that matches of code or the default store
     *
     * @param string $storeCode
     * @return Store
     */
    private function getStore($storeCode = null)
    {
        $result = null;

        $stores = $this->storeManager->getStores();
        foreach ($stores as $storeKey => $store) {
            if ($storeCode) {
                if (strtolower($storeCode) === strtolower($store->getCode())) {
                    $result = $store;
                    break;
                }
            } else {
                if ($store->isDefault()) {
                    $result = $store;
                    break;
                }
            }
        }

        if ($result) {
            $this->logger->debug('PlatformSettingsManager::getStore - ' .
                'getName: ' . $store->getName() . ' | ' .
                'getCode: ' . $store->getCode() . ' | ' .
                'getUrl: ' . $store->getUrl() . ' | ' .
                'isDefault: ' . $store->isDefault() . ' | ' .
                'getFrontendName: ' . $store->getFrontendName() . ' | ' .
                'getIdentities: ' . $store->getIdentities()[0] . ' | ');
        }

        return $result;
    }
}
