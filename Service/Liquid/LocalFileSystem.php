<?php
namespace EkoLabs\Eko\Service\Liquid;

use Keepsuit\Liquid\Contracts\LiquidFileSystem;
use Keepsuit\Liquid\Exceptions\FileSystemException;
use EkoLabs\Eko\Api\FilesystemInterface;

class LocalFileSystem implements LiquidFileSystem
{
    /**
     * UndEkoLocalFileSystem constrctor
     *
     * @param string $root
     * @param FilesystemInterface $filesystem
     * @param string $snippetsPath
     * @param string $pattern
     */
    public function __construct(
        private string $root,
        private FilesystemInterface $filesystem,
        private string $snippetsPath = 'snippets',
        private string $pattern = '%s.liquid'
    ) {
    }

    /**
     * Read a template file from the module's filesystem
     *
     * @param string $templateName
     * @return string
     */
    public function readTemplateFile(string $templateName): string
    {
        $fullPath = $this->fullPath($templateName);

        if (!$this->filesystem->isExists($fullPath)) {
            $fullPath = $this->fullPath(sprintf('%s/%s', $this->snippetsPath, $templateName));
        }
        if (!$this->filesystem->isExists($fullPath)) {
            throw new FileSystemException("Template file '$fullPath' not found");
        }

        $content = $this->filesystem->readFile($fullPath);
        if ($content === false) {
            throw new FileSystemException("Template file '$fullPath' not found");
        }

        return $content;
    }

    /**
     * Get the full path of a template file
     *
     * @param string $templatePath
     * @return string
     */
    public function fullPath(string $templatePath): string
    {
        if (preg_match('{^[a-zA-Z0-9\-_/.]+$}s', $templatePath) === 0) {
            throw new FileSystemException("Illegal template name '$templatePath'");
        }

        if (str_starts_with($templatePath, '/') || str_starts_with($templatePath, '.')) {
            throw new FileSystemException("Illegal template name '$templatePath'");
        }

        if (($templatePath = preg_replace('/\./', '/', $templatePath)) === null) {
            throw new FileSystemException("Illegal template name '$templatePath'");
        }

        $path = match (true) {
            str_contains($templatePath, '/') => sprintf(
                '%s/%s',
                $this->filesystem->getParentDirectory($templatePath),
                sprintf(
                    $this->pattern,
                    $this->filesystem->getBasename($templatePath)
                )
            ),
            default => sprintf(
                $this->pattern,
                $this->filesystem->getBasename($templatePath)
            ),
        };

        return sprintf('%s/%s', $this->root, $path);
    }
}
