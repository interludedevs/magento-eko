<?php
namespace EkoLabs\Eko\Service\Liquid;

use Keepsuit\Liquid\Filters\FiltersProvider;

class CustomFilters extends FiltersProvider
{
    /**
     * A liquid 'json' filter
     *
     * @param array|boolean|integer|string $value
     * @return string
     */
    public function json($value): string
    {
        try {
            if (!$value) {
                return 'false';
            }
            // If the value is already an array or boolean, encode it directly to JSON
            if (is_array($value) || is_bool($value)) {
                return json_encode($value);
            }
            
            // Decode the JSON string to an associative array
            $decodedData = json_decode($value, true);

            // Check if json_decode failed
            if (json_last_error() !== JSON_ERROR_NONE) {
                return json_encode($value);
            }

            // Re-encode the array to a JSON string to ensure it's a valid JSON object
            return json_encode($decodedData);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * A liquid 'first' filer
     *
     * @param array $value
     * @return void
     */
    public function first($value)
    {
        // Check if the value is an array
        if (is_array($value)) {
            // Check if the array is not empty and return the first item
            if (!empty($value)) {
                return reset($value);
            }
        }
        // Return null if the value is not an array or if the array is empty
        return null;
    }

    // phpcs:disable
    /**
     * A liquid 'image_url' filter
     *
     * @param array $input
     * @param int $width
     * @param int $height
     * @return array
     */
    public function image_url($input, $width = null, $height = null)
    {
        if (!$input??['media'] instanceof Media) {
            return $input;
        }
        $media = $input['media'];

        $result = [
            'context'=> [
                'media' => $media,
                'width' => $width,
                'height' => $height
            ],
            'output' => $media->getImageUrl($width, $height)
        ];

        return $result;
    }
    // phpcs:enable

    // phpcs:disable
    /**
     * A liquid 'image_tag' filter
     *
     * @param array $input
     * @param boolean $preload
     * @param string|null $loading
     * @param string|null $fetchpriority
     * @param int|null $width
     * @param int|null $height
     * @param string|null $widths
     * @param string|null $sizes
     * @param string|null $class
     * @return void
     */
    public function image_tag(
        $input,
        $preload = null,
        $loading = null,
        $fetchpriority = null,
        $width = null,
        $height = null,
        $widths = null,
        $sizes = null,
        $class = null
    ) {
        $attributes = [
            'src' => $input['output'],
        ];

        if ($input['context']['width']) {
            $attributes['width'] = $input['context']['width'];
        }

        if ($input['context']['height']) {
            $attributes['height'] = $input['context']['height'];
        }

        if ($preload) {
            $attributes['preload'] = 'preload';
        }

        if ($loading) {
            $attributes['loading'] = $loading;
        }

        if ($fetchpriority) {
            $attributes['fetchpriority'] = $fetchpriority;
        }

        if ($width) {
            $attributes['width'] = $width;
        }

        if ($height) {
            $attributes['height'] = $height;
        }

        if ($class) {
            $attributes['class'] = $class;
        }

        if ($sizes) {
            $attributes['sizes'] = $sizes;
        }

        if ($input['context']['media']?->getAltText()) {
            $attributes['alt'] = $input['context']['media']->getAltText();
        }

        if ($widths) {
            $srcset = [];
            $widthArray = explode(',', $widths);
            foreach ($widthArray as $width) {
                $imageUrl = $input['context']['media']->getImageUrl($width);
                $srcset[] = "$imageUrl {$width}w";
            }
            $attributes['srcset'] = implode(', ', $srcset);
        }

        $attributeString = '';
        foreach ($attributes as $key => $value) {
            $attributeString .= $key . '="' . htmlspecialchars($value, ENT_QUOTES) . '" ';
        }

        return '<img ' . trim($attributeString) . ' />';
    }
    // phpcs:enable
}
