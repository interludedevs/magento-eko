<?php
namespace EkoLabs\Eko\Service\Liquid;

use Keepsuit\Liquid\Contracts\MapsToLiquid;

class MediaContext implements MapsToLiquid, \JsonSerializable
{
    /**
     * @var Image
     */
    private $image;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var ImageHelper
     */
    private $imageHelper;

    /**
     * MediaContext constructor
     *
     * @param Image $image
     * @param Product $product
     * @param ImageHelper $imageHelper
     */
    public function __construct($image, $product, $imageHelper)
    {
        $this->image = $image;
        $this->product = $product;
        $this->imageHelper = $imageHelper;
    }

    /**
     * Return the object for the liquid parser
     *
     * @return mixed
     */
    public function toLiquid(): mixed
    {
        return $this;
    }

    /**
     * Return a JSON serialized array of the media file
     *
     * @return array
     */
    public function jsonSerialize(): mixed
    {
        return [
            'file' => $this->image->getFile()
        ];
    }

    /**
     * Get an image URL of the media
     *
     * @param int|null $width
     * @param int|null $height
     * @return void
     */
    public function getImageUrl($width = null, $height = null)
    {
        return $this->imageHelper->init($this->product, 'product_page_image_large')
            ->setImageFile($this->image->getFile())
            ->resize($width, $height)
            ->getUrl();
    }

    /**
     * Get the alt text of the image
     *
     * @return string
     */
    public function getAltText()
    {
        return $this->image->getLabel();
    }
}
