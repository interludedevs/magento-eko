<?php
namespace EkoLabs\Eko\Service;

use EkoLabs\Eko\Api\VersionInterface;
use Magento\Framework\Composer\ComposerInformation;
use Magento\Framework\Module\Dir;
use Magento\Framework\Filesystem\Driver\File;

class Version implements VersionInterface
{
    private const PACKAGE_NAME = 'ekolabs/magento-eko';
    private const MODULE_NAME = 'EkoLabs_Eko';

    /**
     * @var string
     */
    private $name = 'N/A';

    /**
     * @var string
     */
    private $version = 'N/A';

    /**
     * Constructor
     * @param ComposerInformation $composerInformation
     * @param Dir $moduleDir
     * @param File $fileDriver
     */
    public function __construct(
        ComposerInformation $composerInformation,
        Dir $moduleDir,
        File $fileDriver
    ) {
        // try to get version from ComposerInformation first
        $packages = $composerInformation->getInstalledMagentoPackages();
        if (isset($packages[self::PACKAGE_NAME])) {
            $this->name = $packages[self::PACKAGE_NAME]['name'];
            $this->version = $packages[self::PACKAGE_NAME]['version'];
        } else {
            // if not found, try to read from composer.json
            $composerJsonPath = $moduleDir->getDir(self::MODULE_NAME) . '/composer.json';
            if ($fileDriver->isExists($composerJsonPath)) {
                $composerJsonContent = $fileDriver->fileGetContents($composerJsonPath);
                $composerData = json_decode($composerJsonContent, true);
                if ($composerData) {
                    $this->name = $composerData['name'];
                    $this->version = $composerData['version'];
                }
            }
        }
    }

    /**
     * Get the module's name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the module's version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
}
