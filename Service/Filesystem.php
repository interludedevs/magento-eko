<?php
namespace EkoLabs\Eko\Service;

use EkoLabs\Eko\Api\FilesystemInterface;
use EkoLabs\Eko\Api\LoggerInterface;

use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Module\Dir;

/**
 * A service for handling the module's inner file system
 */
class Filesystem implements FilesystemInterface
{
    private const MODULE_NAME = 'EkoLabs_Eko';

    /**
     * @var File
     */
    private $fileDriver;

    /**
     * @var string
     */
    private $moduleRoot;

    /**
     * @var Name
     */
    private $fileName;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Constructor
     *
     * @param Dir $moduleDir
     * @param File $fileDriver
     * @param LoggerInterface $logger
     */
    public function __construct(
        Dir $moduleDir,
        File $fileDriver,
        LoggerInterface $logger
    ) {
        $this->fileDriver = $fileDriver;
        $this->logger = $logger;
        try {
            $this->moduleRoot = $moduleDir->getDir(self::MODULE_NAME);
        } catch (\Exception $e) {
            $this->moduleRoot = null;
        }
    }

    /**
     * Read a file from the module's filesystem
     *
     * @param string $filename
     * @return string|null
     */
    public function readFile($filename)
    {
        $result = null;
        try {
            $fullPath = $this->getFullPath($filename);
            $this->logger->debug('Filesystem::readFile --- reading= '. $fullPath);
            $result = $this->fileDriver->fileGetContents($fullPath);
        } catch (\Exception $e) {
            $this->logger->debug('Filesystem::readFile --- failed reading file: ' . $e->getMessage());
            $result = null;
        }
        return $result;
    }

    /**
     * Get parent directory's path
     *
     * @param string $path
     * @return string
     */
    public function getParentDirectory($path)
    {
        return $this->fileDriver->getParentDirectory($path);
    }

    /**
     * Get the basename of a path
     *
     * @param string $path
     * @return string
     */
    public function getBasename($path)
    {
        // phpcs:disable
        return basename($path);
        // phpcs:enable
    }

    /**
     * Test is a file from the module's filesystem exists and readbale
     *
     * @param string $filename
     * @return boolean
     */
    public function isExists($filename)
    {
        $result = false;
        try {
            $fullPath = $this->getFullPath($filename);
            $this->logger->debug('Filesystem::isExsists --- reading= '. $fullPath);
            $result = ($this->fileDriver->isExists($fullPath) && $this->fileDriver->isReadable($fullPath));
        } catch (\Exception $e) {
            $this->logger->debug('Filesystem::isExsists --- failed reading file: ' . $e->getMessage());
            $result = false;
        }
        return $result;
    }

    /**
     * Get the full path in the filesystem for a filename in the module's filesystem
     *
     * @param string $filename
     * @return string
     */
    private function getFullPath($filename)
    {
        if ($this->moduleRoot && $filename) {
            return $this->moduleRoot . '/'. $filename;
        }
        return null;
    }
}
