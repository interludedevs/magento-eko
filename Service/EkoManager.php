<?php

declare(strict_types=1);

namespace EkoLabs\Eko\Service;

use EkoLabs\Eko\Api\EkoManagerInterface;
use EkoLabs\Eko\Api\LoggerInterface;
use EkoLabs\Eko\Api\PlatformSettingsManagerInterface;
use EkoLabs\Eko\Api\Data\GalleryObjectInterface;
use EkoLabs\Eko\Model\GalleryObject;
use Magento\Framework\App\Request\Http;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\View\Asset\Repository as AssetRepository;

/**
 * @api
 */
class EkoManager implements EkoManagerInterface
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Http
     */
    private $request;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var PlatformSettingsManagerInterface
     */
    private $platformSettingsManager;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Configurable
     */
    private $configurable;

    /**
     * @var AssetRepository
     */
    private AssetRepository $assetRepository;
    
    /**
     * The Eko configuratin from the config and admin
     *
     * @var array
     */
    private $config;

    /**
     * @param ProductRepositoryInterface $productRepository
     * @param Http $request
     * @param ScopeConfigInterface $scopeConfig
     * @param PlatformSettingsManagerInterface $platformSettingsManager
     * @param StoreManagerInterface $storeManager
     * @param Configurable $configurable
     * @param AssetRepository $assetRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        Http $request,
        ScopeConfigInterface $scopeConfig,
        PlatformSettingsManagerInterface $platformSettingsManager,
        StoreManagerInterface $storeManager,
        Configurable $configurable,
        AssetRepository $assetRepository,
        LoggerInterface $logger
    ) {
        $this->productRepository = $productRepository;
        $this->request = $request;
        $this->scopeConfig = $scopeConfig;
        $this->platformSettingsManager = $platformSettingsManager;
        $this->storeManager = $storeManager;
        $this->configurable = $configurable;
        $this->assetRepository = $assetRepository;
        $this->logger = $logger;
        $this->config = $this->loadConfig();
    }

    /**
     * Get the eko gallery object for the current product if applicable
     *
     * @return GalleryObjectInterface|null
     */
    public function getGalleryObject() : ?GalleryObjectInterface
    {
        $result = null;
        $product = $this->getProduct();
        if ($product) {
            $extensionAttributes = $product->getExtensionAttributes();
            if ($extensionAttributes && $extensionAttributes->getEkoGallery()) {
                $platformSettings = $this->getPlatformSettings();
                $galleryVersion = $platformSettings['gallery']['version'] ?? null;
                $result = new GalleryObject(
                    $extensionAttributes->getEkoGallery(),
                    $this->assetRepository,
                    $galleryVersion
                );
            }
        }
        return $result;
    }

    /**
     * Get the current product object
     *
     * @return \Magento\Catalog\Model\Product|null
     */
    public function getProduct()
    {
        $product = null;
        $productId = $this->request->getParam('id');
        $this->logger->debug('EkoManager::getProduct: ' . $productId);
        if ($this->isProductPage() && $productId) {
            try {
                $currentStoreId = $this->storeManager?->getStore()?->getId();
                $product = $this->productRepository->getById(
                    $productId,
                    false,
                    $currentStoreId
                );
            } catch (Magento\Framework\Exception\NoSuchEntityException $e) {
                $product = null;
            }
        }

        return $product;
    }

    /**
     * Gets the simple products variants of a configurable product
     *
     * @param Product $product
     * @return array|null
     */
    public function getVariants($product)
    {
        if ($product->getTypeId() !== Configurable::TYPE_CODE) {
            return null;
        }

        $simpleProducts = $this->configurable->getUsedProducts($product);
        $currentStoreId = $this->storeManager?->getStore()?->getId();

        $result = [];
        foreach ($simpleProducts as $simpleProduct) {
            // load product with store context
            $storeSimpleProduct = $this->productRepository->getById(
                $simpleProduct->getId(),
                false,
                $currentStoreId
            );
            
            // add store-specific checks
            if ($storeSimpleProduct->isSalable()) {
                $result[] = $storeSimpleProduct;
            }
        };
        return $result;
    }

    /**
     * Try to estimate which is the default variant of a product
     *
     * @param Product $product
     * @return Product|null
     */
    public function getDefaultVariant($product)
    {
        // if the product is not configurable it is the default
        if ($product->getTypeId() !== Configurable::TYPE_CODE) {
            return $product;
        }

        // get the simple products
        $simpleProducts = $this->configurable->getUsedProducts($product);

        // get the default value for the configurable attributes (e.g. color)
        $configurableAttributes = $this->configurable->getConfigurableAttributes($product);
        $defaultValues = [];
        foreach ($configurableAttributes as $attribute) {
            $attributeId = $attribute->getAttributeId();
            $defaultValue = $product->getData('default_' . $attribute->getProductAttribute()->getAttributeCode());
            if ($defaultValue) {
                $defaultValues[$attributeId] = $defaultValue;
            }
        }

        // if default values are set, find the matching simple product
        if (!empty($defaultValues)) {
            foreach ($simpleProducts as $simpleProduct) {
                $match = true;
                foreach ($defaultValues as $attributeId => $value) {
                    if ($simpleProduct->getData($attribute->getProductAttribute()->getAttributeCode()) != $value) {
                        $match = false;
                        break;
                    }
                }
                if ($match) {
                    return $simpleProduct;
                }
            }
        }

        // if no default is set or found, return the first available simple product
        return reset($simpleProducts) ?: null;
    }

    /**
     * Get the Eko configuration
     *
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }
    
    /**
     * Is the Eko rendering conditional?
     *
     * @return boolean
     */
    public function isConditional(): bool
    {
        return $this->config['queryParamEnabled'];
    }

    /**
     * Is the condition defined for Eko rendeing met?
     *
     * @return boolean
     */
    public function isConditionMet(): bool
    {
        $queryParamKey = $this->config['queryParamKey'];
        $queryParamValue = $this->config['queryParamValue'];

        return (
            $queryParamKey &&
            $queryParamValue &&
            $this->request->getParam($queryParamKey) === $queryParamValue
        );
    }

    /**
     * Is eko diabled via the query string param?
     *
     * @return boolean
     */
    public function isDisabled(): bool
    {
        $queryParamKey = $this->config['disabledQueryParamKey'];
        $queryParamValue = $this->config['disabledQueryParamValue'];

        return (
            $queryParamKey &&
            $queryParamValue &&
            $this->request->getParam($queryParamKey) === $queryParamValue
        );
    }

    /**
     * Should the current page be ekofied?
     *
     * @return boolean
     */
    public function isEkofiedPage(): bool
    {
        $config = $this->getConfig();

        $ekoRenderingEnabledForPage = (!$this->isConditional() || $this->isConditionMet());

        $ekoAutoInjectEnabled = (
            $config['autoInjectEnabled'] &&
            $config['autoInjectContainer']
        );

        return (
            $ekoAutoInjectEnabled &&
            $ekoRenderingEnabledForPage &&
            $this->isProductPage() &&
            !$this->isDisabled() &&
            $this->getGalleryObject()
        );
    }

    /**
     * Get the settings published from the Eko platform
     *
     * @return array
     */
    public function getPlatformSettings(): array
    {
        $result = [];
        $storeCode = $this->getCurrentStoreCode();
        $settings = $this->platformSettingsManager->getSettings($storeCode);
        if ($settings) {
            $result = json_decode($settings, true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                $result = [];
            }
        }
        return $result;
    }

    /**
     * Is the current page a product page?
     *
     * @return boolean
     */
    private function isProductPage()
    {
        return $this->request->getFullActionName() === 'catalog_product_view';
    }

    /**
     * Load the Eko config for the current store
     *
     * @return array
     */
    private function loadConfig(): array
    {
        $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $result = [
            'autoInjectEnabled' => (bool)$this->scopeConfig->getValue(
                'eko_gallery/auto_inject/ai_enabled',
                $scope
            ),
            'autoInjectContainer' => $this->scopeConfig->getValue(
                'eko_gallery/auto_inject/ai_container',
                $scope
            ),
            'ekoGalleryBlockName' => $this->scopeConfig->getValue(
                'eko_gallery/auto_inject/ai_gallery_block',
                $scope
            ),
            'autoInjectReplaceBlock' => $this->scopeConfig->getValue(
                'eko_gallery/auto_inject/ai_replace_block',
                $scope
            ),
            'queryParamEnabled' => (bool)$this->scopeConfig->getValue(
                'eko_gallery/auto_inject/ai_query_param_enabled',
                $scope
            ),
            'queryParamKey' => $this->scopeConfig->getValue(
                'eko_gallery/auto_inject/ai_query_param_key',
                $scope
            ),
            'queryParamValue' => $this->scopeConfig->getValue(
                'eko_gallery/auto_inject/ai_query_param_value',
                $scope
            ),
            'disabledQueryParamKey' => $this->scopeConfig->getValue(
                'eko_gallery/auto_inject/ai_disabled_query_param_key',
                $scope
            ),
            'disabledQueryParamValue' => $this->scopeConfig->getValue(
                'eko_gallery/auto_inject/ai_disabled_query_param_value',
                $scope
            ),
            'ekoEnv' => $this->scopeConfig->getValue(
                'eko_gallery/advanced/eko_env',
                $scope
            ),
            'debugLog' => (bool)$this->scopeConfig->getValue(
                'eko_gallery/advanced/ad_debug_log',
                $scope
            )
        ];

        $this->logger->debug('EkoManager::loadConfig', $result);
        return $result;
    }

    /**
     * Get the current store code
     *
     * @return string|null
     */
    private function getCurrentStoreCode()
    {
        return $this->storeManager?->getStore()?->getCode();
    }
}
