<?php
namespace EkoLabs\Eko\Service;

use EkoLabs\Eko\Api\LiquidInterface;

use Keepsuit\Liquid\TemplateFactory;

use EkoLabs\Eko\Service\Liquid\CustomFilters;
use EkoLabs\Eko\Service\Liquid\LocalFileSystem;
use EkoLabs\Eko\Service\Liquid\MediaContext;

use Magento\Catalog\Helper\Image as ImageHelper;
use EkoLabs\Eko\Api\LoggerInterface;
use EkoLabs\Eko\Api\FilesystemInterface;

class Liquid implements LiquidInterface
{
    private const LIQUID_PATH = 'view/frontend/templates/liquid';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ImageHelper
     */
    private $imageHelper;

    /**
     * @var TemplateFactory
     */
    private $templateFactory;

    /**
     * Liquid constructor
     *
     * @param LoggerInterface $logger
     * @param ImageHelper $imageHelper
     * @param FilesystemInterface $filesystem
     * @param array $data
     */
    public function __construct(
        LoggerInterface $logger,
        ImageHelper $imageHelper,
        FilesystemInterface $filesystem,
        array $data = []
    ) {
        $this->logger = $logger;
        $this->imageHelper = $imageHelper;
        $this->templateFactory = TemplateFactory::new()
            ->setFilesystem(new LocalFileSystem(self::LIQUID_PATH, $filesystem))
            ->registerFilter(CustomFilters::class);
    }

    /**
     * Render a liquid template with the provided context object
     *
     * @param string $template
     * @param array $context
     * @return string
     */
    public function render($template, $context)
    {
        
        // Parse from template (loaded from filesystem)
        $template = $this->templateFactory->parseTemplate($template);

        // Create the render context
        $renderContext = $this->templateFactory->newRenderContext(
            environment: $context,
            staticEnvironment: []
        );

        // Render the template
        return $template->render($renderContext);
    }

    /**
     * Create a liquid media object for a magento image of a product
     *
     * @param Image $mediaGalleryImage
     * @param Product $product
     * @return MediaContext
     */
    public function createMedia($mediaGalleryImage, $product)
    {
        return new MediaContext($mediaGalleryImage, $product, $this->imageHelper);
    }
}
