<?php
namespace EkoLabs\Eko\Service;

use EkoLabs\Eko\Api\LoggerInterface as EkoLoggerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;

class Logger implements EkoLoggerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Logger Constructor
     *
     * @param LoggerInterface $logger
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        LoggerInterface $logger,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Send debug message with an optional context array to the logger if logging is enabled
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function debug($message, array $context = [])
    {
        if ($this->isEnabled()) {
            $this->logger->debug($message, $context);
        }
    }

    /**
     * Test is logging is enabled
     *
     * @return boolean
     */
    private function isEnabled()
    {
        return $this->scopeConfig->getValue(
            'eko_gallery/advanced/ad_debug_log',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
